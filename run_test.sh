#!/bin/bash

if [ $CLEAN ]; then
	(cd docker && docker-compose down)
	cargo clean
fi

if [ $BUILD ]; then
	(cd docker && docker-compose down)
	cargo build --target=x86_64-unknown-linux-musl
	cp ./target/x86_64-unknown-linux-musl/debug/troupo ./docker/troupo
	(cd docker && docker-compose up -d --build)
fi


# 
export TROUPO_AUTH=semrBeuQVYGcnouQ6HpD

function fail {
	echo $@
	exit 1
}
function wait_tasks {
	while [ true ]; do
		echo -n "(*) polling tasks..."
		active=$(DOG_JQ_EXPR='.items | map(select(.state == "pending" or .state == "running")) | length' ./dog get task)
		echo " $active active"
		if [[ $active = '0' ]]; then
			return
		fi
		if [[ $active = '' ]]; then
			echo "empty response to task poll, something is wrong"
		fi
		sleep 5
	done
}
function create_instance {
	id=$1
	shift
	name=$1
	shift
	echo "~~~~~ instance creation (name: $name, id: $id) with init+deploy tasks"
	./dog delete instance/$name >/dev/null && echo '(+) previous instance deleted'
	./dog put instance/$name \
		local_serial_id=$id domain=$name.localhost \
		satellites.storage=keyed satellites.postgres=keyed \
		env.AWS_ACCESS_KEY_ID=a env.AWS_SECRET_ACCESS_KEY=b \
		$@ \
		worker_scale.model=Heavy worker_scale.mem_mb=500 worker_scale.threads=2 \
		|| fail "failed to create instance"
	./dog post instance/$name/task task_type=initialize >/dev/null
	./dog post instance/$name/task task_type=deploy >/dev/null
	
	wait_tasks
}

echo '~~~~~ host status'
./dog get / || fail "host status failed"

create_instance 1 test-masto species=mastodon version=mastodon-test 
echo '(*) checking mastodon http'
curl http://localhost:33311/ -s -H "Host: test-masto.localhost" -H "X-Forwarded-Proto: https" | grep "name='description'" || fail "couldnt connecto to mastodon"

create_instance 5 test-pix species=pixelfed version=main 
echo '(*) checking pixelfed http'
curl http://localhost:33301/ -s -H "Host: test-pix.localhost" | grep -o Pixelfed


