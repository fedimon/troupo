#!/bin/bash
if [ $CLEAN ]; then
	(cd docker && docker-compose down)
	cargo clean
fi
cargo build --target=x86_64-unknown-linux-musl
cp ./target/x86_64-unknown-linux-musl/debug/troupo ./docker/troupo
(cd docker && docker-compose up --build)
