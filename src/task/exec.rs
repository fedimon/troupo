use anyhow::{anyhow, Result};
use chrono::Utc;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::task::JoinHandle;

use crate::state::{AppState, TaskReceiver};
use crate::task::{Task, TaskResult, TaskState, TaskType};

pub fn dispatch(state: &Arc<AppState>, task: &Task) -> TaskResult {
    let instance = match state.get_instance(&task.instance) {
        Some(i) => i,
        None => return Err(anyhow::anyhow!("instance {} not found", task.instance).into()),
    };

    crate::species::dispatch_species(state, instance.species, task)
}

/// task managing a single external process, bound to a Task
pub fn task_run(state: Arc<AppState>, task: &Task) {
    // first step is to mark the task as running
    state.mut_task(&task.id, |i| {
        i.state = TaskState::Running;
        i.locally_updated = true;
    });

    log::info!("{}: run {:?}", task.id, task.task_type);
    let start_t = Instant::now();

    // then we can call the actual task code
    let r: TaskResult = dispatch(&state, task);

    let end_t = Instant::now();
    let run_time = (end_t - start_t).as_secs_f64();

    // try to turn into a json number, or a null
    let run_time_value = serde_json::Number::from_f64(run_time)
        .map(|n| serde_json::Value::Number(n))
        .unwrap_or(serde_json::Value::Null);

    // and finally mark the result
    match r {
        Ok(mut r) => {
            log::info!("{}: finished in {:.3}s", task.id, run_time);
            log::trace!("{}: result: {:?}", task.id, r);
            state.mut_task(&task.id, |i| {
                if let Some(map) = r.as_object_mut() {
                    map.insert("run_time".to_owned(), run_time_value);
                }
                i.state = TaskState::Finished;
                i.result = Some(r);
                i.locally_updated = true;
            });
        }
        Err(e) => {
            log::warn!("{}: failed: {:?}", task.id, e);
            let mut result = e.to_json();
            result
                .as_object_mut()
                .unwrap()
                .insert("run_time".to_owned(), run_time_value);
            state.mut_task(&task.id, |i| {
                i.state = TaskState::Failed;
                i.result = Some(result);
                i.locally_updated = true;
            });
        }
    }
}

pub async fn task_watcher(state: Arc<AppState>) -> Result<()> {
    let period = std::time::Duration::from_millis(500);

    log::debug!("starting task watcher (period: {period:?})");

    let mut interval = tokio::time::interval(period);
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    loop {
        // periodically re-queues pending tasks
        let to_enqueue = state.runtime_config.get_with(|c| {
            let mut r = Vec::new();
            for task in c.tasks.values() {
                if task.state == TaskState::Pending {
                    r.push(task.id);
                }
            }
            r
        });

        // something should be done with running tasks right?
        // like ensure it's actually running, handle troupo restarts
        // amd possibly a task that panicked?
        // TODO

        for id in to_enqueue.into_iter() {
            state.task_queue.send(id).await;
        }

        let _tick = interval.tick().await;
    }
}

/// in here we poll the receiver (mpsc queue) and start new tasks when
/// it's time to do so.
/// this task & queue control how fast and chaotically tasks are processed:
/// it ensures only one task is running per instance at any time
pub async fn task_distributor(state: Arc<AppState>, mut receiver: TaskReceiver) -> Result<()> {
    log::debug!("starting task distributor");
    // JoinHandles are added to their instance's job queue when started.
    // if one is already running, tasks are ignored and will be re-queued
    // periodically later by the watch task
    let mut instance_jobs: HashMap<String, JoinHandle<()>> = HashMap::new();

    loop {
        let new_id = match receiver.recv().await {
            Some(id) => id,
            None => {
                log::warn!("execution queue channel closed :(");
                return Err(anyhow!("execution queue closed"));
            }
        };

        let task = match state.get_task(new_id) {
            Some(t) => t,
            None => {
                log::warn!("{}: (from pending queue) task not found", new_id);
                continue;
            }
        };

        if task.state != TaskState::Pending {
            log::trace!("{}: skipped (currently {:?})", task.id, task.state);
            continue;
        }

        if let Some(handle) = instance_jobs.get(&task.instance) {
            if !handle.is_finished() {
                log::trace!("{}: skipped (instance {} busy)", task.id, task.instance);
                continue;
            }
        }

        let state_copy = state.clone();
        let instance_name = task.instance.clone();
        let handle = tokio::task::spawn_blocking(move || {
            task_run(state_copy, &task);
        });
        instance_jobs.insert(instance_name, handle);
    }
}

/// Delete old and finished tasks
async fn prune_tasks(state: &Arc<AppState>) {
    log::trace!("scheduler: pruning tasks");
    let mut counter: i32 = 0;
    state.runtime_config.mut_with(|config| {
        config.tasks.retain(|_key, task| {
            // never prune pending tasks
            if task.state != TaskState::Finished && task.state != TaskState::Failed {
                return true;
            }

            // tasks expire after 24h
            // TODO: add to config
            let ttl = chrono::Duration::hours(24);
            let last_t = task.update_date.as_ref().unwrap_or(&task.create_date);
            let now = Utc::now();
            let expired = now.signed_duration_since(*last_t) > ttl;
            counter = counter + (expired as i32);

            !expired
        })
    });

    if counter > 0 {
        log::debug!("pruned {counter} expired tasks");
    }
}

fn run_scheduled_tasks() {}
fn run_scheduled_instance_tasks(state: &Arc<AppState>, inst_name: &str) {
    // the whole thing is obsolete. periodic tasks are scheduled by the registry
    // as to spread load better, and that restart was making a mess
    return;

    let media_delay = chrono::Duration::days(7);
    let restart_delay = chrono::Duration::days(2);
    // TODO into config

    let mut tasks = Vec::new();
    state.mut_instance(inst_name, |inst| {
        if inst.periodic_tasks.run("media_prune", media_delay) {
            tasks.push(TaskType::PruneCaches);
        }
        if inst.periodic_tasks.run("restart", restart_delay) {
            tasks.push(TaskType::Deploy);
        }
    });

    for t in tasks {
        log::debug!("scheduler: queueing periodic task {t:?} for {inst_name}");
        let task = Task::new(inst_name, t);
        state.add_task(&task);
    }
}

fn hash_offset(i: &str) -> usize {
    use std::hash::{Hash, Hasher};

    let mut h = std::collections::hash_map::DefaultHasher::new();
    i.hash(&mut h);
    h.finish() as usize
}

pub fn update_stats(state: &Arc<AppState>, counter: usize) {
    // for performances and task list upkeep, we create a temporary Task
    // that will not be saved beyond each run.
    // a regular update_stats task is still schedulable
    for inst_name in state.list_instances_with_scheduler() {
        // instance names are used to 'randomly' spread full stats taking
        // over a 15 minutes period
        let full = (counter + hash_offset(&inst_name)) % 15 == 0;

        let mut task = Task::new(&inst_name, TaskType::UpdateStats);
        task.parameters
            .insert("full".to_owned(), serde_json::Value::Bool(full));

        if let Err(e) = dispatch(state, &task) {
            log::warn!("stats update for {} failed: {:?}", inst_name, e);
        }
    }
}

/// Spawn periodic tasks at a fixed interval
pub async fn task_scheduler(state: Arc<AppState>) -> Result<()> {
    let mut period = tokio::time::interval(Duration::from_secs(60));
    let mut counter = 0;
    loop {
        prune_tasks(&state).await;

        let state2 = state.clone();
        tokio::task::spawn_blocking(move || {
            update_stats(&state2, counter);
        });

        // attempt the heavier tasks once per hour
        // after 33minutes to avoid stressing at startup
        if counter % 60 == 33 {
            log::debug!("scheduler: scanning for periodic tasks");
            for name in state.list_instances_with_scheduler() {
                run_scheduled_instance_tasks(&state, &name);
            }
            run_scheduled_tasks();
        }

        counter += 1;
        period.tick().await;
    }
}

/// Spawn all background tasks required for task processing and maintenance.
/// this is meant to be ran once from main()
pub fn boot(state: &Arc<AppState>, receiver: TaskReceiver) {
    tokio::task::spawn(task_distributor(state.clone(), receiver));
    tokio::task::spawn(task_watcher(state.clone()));
    tokio::task::spawn(task_scheduler(state.clone()));
}
