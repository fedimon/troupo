use anyhow::{anyhow, Result};
use chrono::{DateTime, Utc};
use serde::{de::Visitor, Deserialize, Deserializer, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use std::sync::Arc;
use uuid::Uuid;


pub type TaskOutput = Vec<(String, String)>;

pub fn nullable_output<'de, D>(deserializer: D) -> Result<TaskOutput, D::Error>
where
    D: Deserializer<'de>,
{
    let opt = Option::deserialize(deserializer)?;
    Ok(opt.unwrap_or_else(TaskOutput::new))
}


#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq, FromFormField)]
#[serde(rename_all = "snake_case")]
pub enum TaskType {
    /// Updates local services to the current configuration
    #[field(value = "deploy")]
    Deploy,
    /// Removes local services and environment file
    #[field(value = "remove")]
    Remove,
    #[field(value = "check")]
    Check,
    #[field(value = "restart")]
    Restart,
    #[field(value = "initialize")]
    Initialize,
    #[field(value = "command")]
    Command,

    #[field(value = "prepare_upgrade")]
    PrepareUpgrade,
    #[field(value = "finish_upgrade")]
    FinishUpgrade,

    /// Creates a Mastodon user
    #[field(value = "create_user")]
    CreateUser,
    /// Promotes a Mastodon user
    #[field(value = "promote_user")]
    PromoteUser,
    ///
    #[field(value = "export")]
    Export,
    #[field(value = "prune_caches")]
    PruneCaches,

    #[field(value = "update_stats")]
    UpdateStats,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
struct TypeContainer {
    val: TaskType,
}

impl std::str::FromStr for TaskType {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // [:
        toml::from_str(&format!("val = \"{s}\""))
            .map(|c: TypeContainer| c.val)
            .map_err(|e| format!("{e}"))
    }
}

#[test]
fn test_type_parse() {
    let c = TypeContainer {
        val: TaskType::Export,
    };
    assert_eq!(toml::to_string(&c).unwrap(), "val = \"export\"\n");
    assert_eq!(
        toml::from_str::<TypeContainer>("val = \"export\"").unwrap(),
        c
    );

    assert_eq!("export".parse::<TaskType>().unwrap(), TaskType::Export);
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq, FromFormField)]
#[serde(rename_all = "snake_case")]
pub enum TaskState {
    /// Task has been received by troupo, but has not yet started
    Pending,
    /// Task is currently running
    Running,
    /// Task has finished successfully (see `results` for the output)
    Finished,
    /// Task has failed (see `result` for details)
    Failed,

    Requested,
    Cancelled,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Task {
    pub id: Uuid,
    pub instance: String,
    pub task_type: TaskType,
    pub state: TaskState,
    pub create_date: DateTime<Utc>,
    pub update_date: Option<DateTime<Utc>>,
    pub parameters: HashMap<String, serde_json::Value>,
    pub result: Option<serde_json::Value>,
    /// a map of subcommands to their output
    /// split off of result as it's built progressively, not at the end.
    /// in contrast it's also more for diagnostics than the structured result
    #[serde(default, deserialize_with = "nullable_output")]
    pub output: Vec<(String, String)>,
    /// an internal field to keep track of individual subcommands
    #[serde(default)]
    pub subcommand_id: usize,
    /// an internal flag to keep track of local changes and what to push to
    /// the registry
    #[serde(default)]
    pub locally_updated: bool,
}

impl Task {
    pub fn new(instance: &str, task_type: TaskType) -> Self {
        Task {
            id: uuid::Uuid::new_v4(),
            instance: instance.to_owned(),
            task_type,
            state: TaskState::Pending,
            create_date: Utc::now(),
            update_date: None,
            parameters: HashMap::new(),
            result: None,
            output: Default::default(),
            subcommand_id: 0,
            locally_updated: false,
        }
    }
    pub fn with_params(mut self, params: HashMap<String, serde_json::Value>) -> Self {
        self.parameters = params;
        self
    }
    pub async fn enqueue(&self, state: &Arc<crate::state::AppState>) {
        state.add_task(self);
        crate::state::on_task_add(state.clone(), self).await;
    }
}

// an error that also embeds task output
#[derive(Debug)]
pub struct TaskError {
    pub code: usize,
    pub error: anyhow::Error,
    pub output: String,
}
impl<T> From<T> for TaskError
where
    T: Into<anyhow::Error>,
{
    fn from(e: T) -> Self {
        Self {
            code: 0,
            error: e.into(),
            output: String::new(),
        }
    }
}
impl TaskError {
    pub fn new(error: anyhow::Error, output: String) -> Self {
        Self {
            error,
            output,
            code: 0,
        }
    }
    pub fn command(exit_code: usize, output: String) -> Self {
        Self {
            code: exit_code,
            error: anyhow::anyhow!("command returned code {exit_code}"),
            output: output,
        }
    }
    pub fn to_json(&self) -> serde_json::Value {
        serde_json::json!({
            "error": format!("{:#}", self.error),
            "error_trace": format!("{:?}", self.error),
            "output": self.output,
        })
    }
}
impl std::fmt::Display for TaskError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.error)
    }
}

pub type TaskResult<T = serde_json::Value> = Result<T, TaskError>;

pub struct TaskParams<'a>(&'a HashMap<String, Value>);
impl<'a> TaskParams<'a> {
    pub fn new(task: &'a Task) -> Result<Self> {
        Ok(Self(&task.parameters))
    }
    pub fn req(&'a self, key: &str) -> Result<&'a Value> {
        self.0.get(key).ok_or(anyhow!("missing '{key}' param"))
    }
    pub fn req_str(&'a self, key: &str) -> Result<&'a str> {
        self.req(key)?
            .as_str()
            .ok_or(anyhow!("failed to convert '{key}' param"))
    }
}
