use crate::config::{InstanceConfig, InstanceStatus, Species, TaskRegistry, WebScale, WorkerScale};
use crate::state::AppState;
use crate::task::{Task, TaskState};
use anyhow::{anyhow, Result, bail};
use chrono::prelude::*;
use serde::{Deserialize, Deserializer, Serialize};
use serde_json::json;
use std::collections::HashMap;
use std::sync::Arc;

use super::TaskType;

fn nullable_map<'de, D>(deserializer: D) -> Result<HashMap<String, serde_json::Value>, D::Error>
where
    D: Deserializer<'de>,
{
    let opt = Option::deserialize(deserializer)?;
    Ok(opt.unwrap_or_else(HashMap::new))
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RegistryInstance {
    pub name: String,
    pub state: String,
    pub domain: String,
    pub cluster: Option<String>,
    pub hosts: Vec<String>,
    pub branch: String,
    pub species: Species,
    pub run_scheduler: bool,
    pub cancel_type: Option<String>,
    #[serde(default, deserialize_with = "nullable_map")]
    pub stats: HashMap<String, serde_json::Value>,
    #[serde(default, deserialize_with = "nullable_map")]
    pub status_data: HashMap<String, serde_json::Value>,
    pub satellites: HashMap<String, String>,
    pub port_number_index: u16,
    pub parameters: HashMap<String, String>,
    pub last_change: Option<DateTime<Utc>>,
    pub last_sync: Option<DateTime<Utc>>,
    pub web_scale: WebScale,
    pub worker_scale: WorkerScale,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RegistryTask {
    pub id: uuid::Uuid,
    pub task_type: TaskType,
    pub state: TaskState,
    pub create_date: DateTime<Utc>,
    pub update_date: Option<DateTime<Utc>>,
    pub parameters: HashMap<String, serde_json::Value>,
    pub result: Option<serde_json::Value>,
    #[serde(default, deserialize_with = "crate::task::def::nullable_output")]
    pub output: Vec<(String, String)>,
}

/// a task with an embedde Instance member
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TaskExt {
    #[serde(flatten)]
    pub task: RegistryTask,

    pub instance: RegistryInstance,
}

impl TaskExt {
    pub fn to_internal_task(&self) -> Task {
        Task {
            id: self.task.id,
            instance: self.instance.name.clone(),
            task_type: self.task.task_type,
            state: self.task.state,
            create_date: self.task.create_date,
            update_date: self.task.update_date,
            parameters: self.task.parameters.clone(),
            result: self.task.result.clone(),
            output: self.task.output.clone(),
            subcommand_id: Default::default(),
            locally_updated: false,
        }
    }
}
impl RegistryInstance {
    pub fn to_internal_instance(&self) -> InstanceConfig {
        InstanceConfig {
            name: self.name.clone(),
            local_id: self.port_number_index,
            species: self.species,
            version: self.branch.clone(),
            domain: self.domain.clone(),
            web_scale: self.web_scale.clone(),
            worker_scale: self.worker_scale.clone(),
            run_scheduler: self.run_scheduler,
            env: self.parameters.clone(),
            satellites: self.satellites.clone(),
            // they are generated here, and we dont need to restore values.
            stats: Default::default(),
            // we might need those values, from_json_map will extract whatever
            // we recognize
            status: InstanceStatus::from_json_map(&self.status_data),
            periodic_tasks: Default::default(),
            locally_updated: false,
        }
    }
}

/// instances are pulled first
pub async fn pull_instances(state: &Arc<AppState>, task_registry: &TaskRegistry) -> Result<()> {
    let client = reqwest::Client::new();
    let page: Vec<RegistryInstance> = client
        .get(format!("{}/instances/", task_registry.url))
        .header("X-Host-Token", &task_registry.auth)
        .send()
        .await?
        .json()
        .await
        .map_err(|e| anyhow!("failed to decode registry response: {e}"))?;

    for item in page {
        // update known instance
        let changed = state.mut_instance(&item.name, |i| {
            i.env = item.parameters.clone();
            i.satellites = item.satellites.clone();
            i.species = item.species;
            i.version = item.branch.clone();
            i.web_scale = item.web_scale.clone();
            i.worker_scale = item.worker_scale.clone();
        });
        match changed {
            Some(_instance) => {
                log::trace!("pulled instance {} (known)", item.name);
            }
            None => {
                log::debug!("pulled instance {} (new)", item.name);

                let inst = item.to_internal_instance();
                state.runtime_config.mut_with(|c| {
                    c.instances.insert(item.name.to_owned(), inst);
                });
            }
        }
    }

    Ok(())
}

/// tasks are pulled from the registry to immediately get pending tasks on startup
pub async fn pull_tasks(state: &Arc<AppState>, task_registry: &TaskRegistry) -> Result<()> {
    let client = reqwest::Client::new();
    let page: Vec<TaskExt> = client
        .get(format!("{}/tasks/?include_instance=1", task_registry.url))
        .header("X-Host-Token", &task_registry.auth)
        .send()
        .await?
        .json()
        .await
        .map_err(|e| anyhow!("failed to decode registry response: {e}"))?;

    for mut item in page {
        match state.get_task(item.task.id) {
            Some(_existing_task) => {
                // compare existing_task and item.task
                // i don't think there's anything to do here now.
                // handle cancellation?
            }
            None => {
                if ![TaskState::Running, TaskState::Pending].contains(&item.task.state) {
                    // skip states that arent incoming
                    continue;
                } 

                // skip if we dont know the instance (it will be left for a future tick)
                let inst = state.get_instance(&item.instance.name);
                if inst.is_none() {
                    log::debug!(
                        "pulled task for unknown inst '{}', skipping",
                        item.instance.name
                    );
                }

                // tasks wont be retried faster than this even if they are marked
                // as pending again. a retry should generate a new task id, to
                // differenciate trials, and avoid getting into a loop due to broken
                // synchronization
                let retry_cooldown = chrono::Duration::minutes(15);

                // create task
                if item.task.state == TaskState::Pending {
                } else if item.task.state == TaskState::Running {
                    let d = item.task.update_date.unwrap_or(item.task.create_date);
                    if d.signed_duration_since(Utc::now()) < retry_cooldown {
                        log::trace!("ignoring {:?} task {}", item.task.state, item.task.id);
                        continue;
                    }
                    // reset to pending; it was lost and is in time for 'implicit' retry
                    item.task.state = TaskState::Pending;
                } else {
                    //log::trace!("ignoring {:?} task {}", item.task.state, item.task.id);
                    continue;
                }

                // convert the RegistryTask to an internal Task object
                let new_task = item.to_internal_task();
                state.add_task(&new_task);
                crate::state::on_task_add(state.clone(), &new_task).await;
            }
        }
    }

    Ok(())
}

/// push updates back to the registry
pub async fn push_task(state: &Arc<AppState>, id: &uuid::Uuid) -> Result<()> {
    let task_registry = match &state.startup_config.task_registry {
        Some(c) => c.clone(),
        None => anyhow::bail!("no registry configured"),
    };

    let task = state
        .get_task(id.clone())
        .ok_or_else(|| anyhow!("push requested but task not found {id}"))?;

    log::trace!("pushing task {id} state={:?}", task.state);

    let client = reqwest::Client::new();
    let r = client
        .patch(format!("{}/tasks/{}/", task_registry.url, task.id))
        .header("X-Host-Token", task_registry.auth)
        .body(
            serde_json::to_string(&json!({
                "state": task.state,
                "result": task.result,
                "output": task.output,
            }))
            .unwrap(),
        )
        .send()
        .await?;

    if r.status() != 200 {
        bail!("push task {id}: received code {}", r.status());
    }

    // mark as synchronized
    state.mut_task(id, |i| i.locally_updated = false);

    Ok(())
}

/// push instance updates back to the registry
pub async fn push_instance(state: &Arc<AppState>, name: &str) -> Result<()> {
    let task_registry = match &state.startup_config.task_registry {
        Some(c) => c.clone(),
        None => anyhow::bail!("no registry configured"),
    };

    let inst = state
        .get_instance(name)
        .ok_or_else(|| anyhow!("push requested but instance not found '{name}'"))?;

    log::trace!("pushing inst '{name}'");

    let client = reqwest::Client::new();
    let r = client
        .patch(format!("{}/instances/{}/", task_registry.url, name))
        .header("X-Host-Token", task_registry.auth)
        .body(
            serde_json::to_string(&json!({
                "stats": inst.stats,
                "status_data": inst.status,
            }))
            .unwrap(),
        )
        .send()
        .await?;

    if r.status() != 200 {
        bail!("push inst '{name}': received code {}", r.status());
    }

    // mark as synchronized
    state.mut_instance(name, |i| i.locally_updated = false);

    Ok(())
}

/// important: we always need to pull all instances before running any task.
/// tasks should not be ran with an incomplete local instance list, as some services
/// (pgbouncer) will require the full list during setup or break existing instances.
pub async fn puller(state: Arc<AppState>, task_registry: TaskRegistry) -> Result<()> {
    let period = task_registry.pull_interval;

    let mut interval = tokio::time::interval(period);
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    loop {
        match pull_instances(&state, &task_registry).await {
            Ok(_) => {}
            Err(e) => {
                log::warn!("registry error pulling instances: {}", e);

                // only continue with tasks if instances are up to date
                let _tick = interval.tick().await;
                continue;
            }
        }
        match pull_tasks(&state, &task_registry).await {
            Ok(_) => {}
            Err(e) => {
                log::warn!("registry error pulling tasks: {}", e);
            }
        }

        let _tick = interval.tick().await;
    }
}

/// push tasks & instances tagged for push
/// atm it's only about the .status and .stats structures (for the rest
/// the registry is the authority) (tasks are pushed)
pub async fn pusher(state: Arc<AppState>, task_registry: TaskRegistry) -> Result<()> {
    let period = task_registry.push_interval;

    let mut interval = tokio::time::interval(period);
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    loop {
        // list instances and tasks that need pushing
        let instances: Vec<String> = state.runtime_config.get_with(|rc| {
            let mut out = Vec::new();
            for (k, v) in rc.instances.iter() {
                if !v.locally_updated {
                    continue;
                }
                out.push(k.to_owned());
            }
            out
        });
        let tasks: Vec<uuid::Uuid> = state.runtime_config.get_with(|rc| {
            let mut out = Vec::new();
            for (k, v) in rc.tasks.iter() {
                if !v.locally_updated {
                    continue;
                }
                out.push(k.to_owned());
            }
            out
        });

        if !instances.is_empty() || !tasks.is_empty() {
            log::debug!(
                "pushing tasks interval={}s instances={} tasks={}",
                period.as_secs(),
                instances.len(),
                tasks.len()
            );
        }

        for name in instances.into_iter() {
            match push_instance(&state, &name).await {
                Ok(_) => {}
                Err(e) => {
                    log::warn!("error pushing inst '{name}' to registry: {}", e);
                }
            }
        }
        for id in tasks.into_iter() {
            match push_task(&state, &id).await {
                Ok(_) => {}
                Err(e) => {
                    log::warn!("error pushing task {id} to registry: {e}");
                }
            }
        }

        let _tick = interval.tick().await;
    }
}

pub fn spawn_sync_tasks(state: Arc<AppState>) {
    let config = match &state.startup_config.task_registry {
        Some(c) => c.clone(),
        None => return,
    };

    tokio::task::spawn(puller(state.clone(), config.clone()));
    tokio::task::spawn(pusher(state, config));
}
