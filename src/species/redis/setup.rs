use crate::land::*;
use crate::state::AppState;
use crate::task::def::TaskResult;
use crate::task::Task;
use anyhow::{anyhow, Context};
use std::path::PathBuf;
use std::sync::Arc;

fn build_redis_unit(identifier: &str) -> String {
    format!(
        r#"
[Unit]
Description=Advanced key-value store
After=network.target
Documentation=http://redis.io/documentation, man:redis-server(1)

[Service]
Type=notify
ExecStart=/usr/bin/redis-server /etc/redis/redis-{identifier}.conf --supervised systemd --daemonize no
PIDFile=/run/redis/redis-server-{identifier}.pid
TimeoutStopSec=0
Restart=always
User=redis
Group=redis
RuntimeDirectory=redis
RuntimeDirectoryMode=2755

UMask=007
PrivateTmp=true
LimitNOFILE=65535
PrivateDevices=true
ProtectHome=true
ProtectSystem=strict
ReadWritePaths=-/var/lib/redis
ReadWritePaths=-/var/log/redis
ReadWritePaths=-/var/run/redis

CapabilityBoundingSet=
LockPersonality=true
MemoryDenyWriteExecute=true
NoNewPrivileges=true
PrivateUsers=true
ProtectClock=true
ProtectControlGroups=true
ProtectHostname=true
ProtectKernelLogs=true
ProtectKernelModules=true
ProtectKernelTunables=true
ProtectProc=invisible
RemoveIPC=true
RestrictAddressFamilies=AF_INET AF_INET6 AF_UNIX
RestrictNamespaces=true
RestrictRealtime=true
RestrictSUIDSGID=true
SystemCallArchitectures=native
SystemCallFilter=@system-service
SystemCallFilter=~ @privileged @resources

# This restricts this service from executing binaries other than redis-server
# itself. This is really effective at e.g. making it impossible to an
# attacker to spawn a shell on the system, but might be more restrictive
# than desired. If you need to, you can permit the execution of extra
# binaries by adding an extra ExecPaths= directive with the command
# systemctl edit redis-server.service
NoExecPaths=/
ExecPaths=/usr/bin/redis-server /usr/lib /lib

[Install]
WantedBy=multi-user.target
Alias=redis.service
"#
    )
}

fn build_redis_conf(identifier: &str, password: &str, port: u16) -> String {
    format!(
        r#"
bind 127.0.0.1
port {port}
timeout 0
pidfile /run/redis/redis-server-{identifier}.pid
loglevel notice
syslog-enabled yes
syslog-ident redis-{identifier}
databases 1
always-show-logo no
save 900 1
save 300 10
save 60 10000
protected-mode yes
requirepass {password}
stop-writes-on-bgsave-error yes
rdbcompression yes
rdbchecksum yes
dbfilename dump-{identifier}.rdb
rdb-del-sync-files no
dir /var/lib/redis/{identifier}/
replica-serve-stale-data yes
replica-read-only yes
repl-diskless-sync no
repl-diskless-sync-delay 5
repl-diskless-load disabled
repl-disable-tcp-nodelay no
replica-priority 100
acllog-max-len 128
lazyfree-lazy-eviction no
lazyfree-lazy-expire no
lazyfree-lazy-server-del no
replica-lazy-flush no
lazyfree-lazy-user-del no
oom-score-adj no
oom-score-adj-values 0 200 800
appendonly no
appendfilename "appendonly.aof"
appendfsync everysec
no-appendfsync-on-rewrite no
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated yes
aof-use-rdb-preamble yes
lua-time-limit 5000
slowlog-log-slower-than 10000
slowlog-max-len 128
latency-monitor-threshold 0
notify-keyspace-events ""
hash-max-ziplist-entries 512
hash-max-ziplist-value 64
list-max-ziplist-size -2
list-compress-depth 0
set-max-intset-entries 512
zset-max-ziplist-entries 128
zset-max-ziplist-value 64
hll-sparse-max-bytes 3000
stream-node-max-bytes 4096
stream-node-max-entries 100
activerehashing yes
client-output-buffer-limit normal 0 0 0
client-output-buffer-limit replica 256mb 64mb 60
client-output-buffer-limit pubsub 32mb 8mb 60
hz 10
dynamic-hz yes
aof-rewrite-incremental-fsync yes
rdb-save-incremental-fsync yes
jemalloc-bg-thread yes
"#
    )
}

use crate::env_builder::keygen_password;

pub fn instance_setup<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();

    let inst_name = &task.instance;
    let svc = format!("redis-{inst_name}-main.service");
    let inst = state.get_instance(inst_name).unwrap();

    let port = 10_000 + inst.local_id;
    let pass = inst
        .greate_secret(state, "REDIS_PASSWORD", keygen_password)
        .context("inst redis password")?;

    // build systemd service
    let redis_svc_path = PathBuf::from(format!(
        "/etc/systemd/system/redis-{inst_name}-main.service"
    ));
    let redis_unit = build_redis_unit(&inst_name);
    changes.push(
        T::set_text(&redis_svc_path, &redis_unit)
            .with_context(|| format!("writing redis svc for inst {inst_name}"))?,
        &format!("redis-{inst_name}-main.service"),
    );

    // build redis conf
    T::mkdir(&PathBuf::from(format!("/var/lib/redis/{inst_name}/")))?;
    let redis_conf_path = PathBuf::from(format!("/etc/redis/redis-{inst_name}.conf"));
    let redis_conf = build_redis_conf(&inst_name, &pass, port);
    changes.push(
        T::set_text(&redis_conf_path, &redis_conf)
            .with_context(|| format!("writing redis conf for inst {inst_name}"))?,
        &format!("{redis_conf_path:?}"),
    );

    // start service
    T::run_as_root(state, task, &["systemctl".into(), "daemon-reload".into()])?;
    if T::svc_is_enabled(state, task, &svc)? {
        // note: this is only start, not restart. should it be restarted under
        // some circumstances?
        T::run_as_root(state, task, &["systemctl", "start", &svc])?;
    } else {
        T::run_as_root(state, task, &["systemctl", "enable", "--now", &svc])?;
    }

    Ok(changes)
}

pub fn instance_remove<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();

    let inst_name = &task.instance;
    let svc = format!("redis-{inst_name}-main.service");

    let svc_path = PathBuf::from(format!("/etc/systemd/system/{svc}.service"));

    T::run_as_root(state, task, &["systemctl", "disable", "--now", &svc])?;

    changes.push(T::delete(&svc_path)?, &format!("delete service {svc}"));

    Ok(changes)
}
