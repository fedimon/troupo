use crate::land::*;
use crate::state::AppState;
use crate::task::def::{TaskError, TaskParams, TaskResult};
use crate::task::Task;
use anyhow::{anyhow, Context};
use std::sync::Arc;

use super::Animal;

pub mod setup;

use setup::{instance_remove, instance_setup};

pub struct Redis {}
impl Animal for Redis {
    fn deploy(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let _check_result = Self::check(state, task)?;
        let changes = instance_setup::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn remove(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let _check_result = Self::check(state, task)?;
        let changes = instance_remove::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn check(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let changes = instance_setup::<LocalCheckActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
}
