use crate::env_builder::EnvBuilder;
use crate::land::*;
use crate::land::{get_bin_path, serialize_env};
use crate::state::AppState;
use crate::task::def::{TaskError, TaskParams, TaskResult};
use crate::task::Task;
use anyhow::{anyhow, Context};
use chrono::Duration;
use redis::Commands;
use regex::Regex;
use std::process::Command;
use std::sync::Arc;

use super::Animal;

pub mod env;
pub mod setup;

use setup::{instance_remove, instance_setup};

fn check_bool(v: Option<&serde_json::Value>) -> bool {
    match v {
        Some(serde_json::Value::Bool(b)) => *b,
        Some(serde_json::Value::String(s)) => s.to_lowercase() == "true",
        _ => false,
    }
}

fn prune_wrapper(
    state: &Arc<AppState>,
    task: &Task,
    command: &[&str],
    regex: &str,
) -> TaskResult<(Option<i32>, Option<String>)> {
    let bundle_path = get_bin_path(state, &task.instance, "bundle");
    let sub = TaskRunner::as_user(state, &task)?;

    let mut full_command = vec![
        bundle_path.as_str(),
        "exec",
        "env",
        "DEFAULT_LOCALE=en",
        "bin/tootctl",
    ];
    full_command.extend_from_slice(command);

    let code = sub.run(&full_command)?;
    let output = sub.output()?;
    if code != 0 {
        return Err(TaskError::command(code, output));
    }
    let re = Regex::new(regex).unwrap();
    let media = re.captures(&output).and_then(|c| {
        let n = c
            .get(1)
            .and_then(|m| m.as_str().parse().ok() as Option<i32>);
        let b = c.get(2).map(|m| m.as_str().to_owned());
        Some((n, b))
    });
    let media = media.ok_or(TaskError::new(
        anyhow::anyhow!("failed to parse output"),
        output,
    ));
    TaskResult::from(media)
}

fn clear_db_lock(conf: &PgConfig) -> TaskResult<()> {
    LocalWriteActor::with_db_admin(conf, |db| {
        db.execute("select pg_terminate_backend(pid) from pg_locks where database=(select oid from pg_database where datname=$1) and mode='ExclusiveLock';",
                &[&conf.dbname],
            )?;
        Ok(())
    })?;
    Ok(())
}

pub struct Mastodon {}
impl Animal for Mastodon {
    fn deploy(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let _check_result = Self::check(state, task)?;
        let changes = instance_setup::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn remove(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let _check_result = Self::check(state, task)?;
        let changes = instance_remove::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn check(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let changes = instance_setup::<LocalCheckActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn restart(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let inst_name = &task.instance;

        for svc_key in setup::SERVICES {
            let svc = format!("mastodon-{inst_name}-{svc_key}.service");
            LocalWriteActor::run_as_root(state, task, &["systemctl", "restart", &svc])?;
        }

        Ok(serde_json::json!({}))
    }
    fn command(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let params = TaskParams::new(task)?;
        let cmd = params.req_str("cmd")?;

        let mut cmd = shlex::split(cmd).ok_or(anyhow!("failed to parse command input"))?;

        if cmd.get(0).map(String::as_str) == Some("tootctl") {
            cmd[0] = "bin/tootctl".to_string();
        }

        let bundle_path = get_bin_path(state, &task.instance, "bundle");
        let mut args: Vec<&str> = vec![&bundle_path, "exec"];
        args.extend(cmd.iter().map(String::as_str));

        let sub = TaskRunner::as_user(state, &task)?;
        let code = sub.run(&args)?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }

        Ok(serde_json::json!({
            "output": output,
            "code": code,
        }))
    }
    fn initialize(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let bundle_path = get_bin_path(state, &task.instance, "bundle");

        // run intial deploy before doing anything else
        let _changes = instance_setup::<LocalWriteActor>(state, task)?;

        let sub = TaskRunner::as_user(state, &task)?;
        sub.check_run(&[&bundle_path, "exec", "rails", "db:schema:load"], 0)?;
        sub.check_run(&[&bundle_path, "exec", "rails", "db:seed"], 0)?;

        Ok(serde_json::json!({}))
    }
    fn prepare_upgrade(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let bundle_path = get_bin_path(state, &task.instance, "bundle");
        let sub = TaskRunner::as_user(state, &task)?;
        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;

        let pg_config = PgConfig::from_inst(state, &instance)?;
        clear_db_lock(&pg_config)?;

        // overwrite db config to bypass pgbouncer for migrations

        sub.check_run(
            &[
                "-E",
                &format!("DB_HOST={}", pg_config.host),
                "-E",
                &format!("DB_PORT={}", pg_config.port),
                "-E",
                "SKIP_POST_DEPLOYMENT_MIGRATIONS=true",
                &bundle_path,
                "exec",
                "rails",
                "db:migrate",
            ],
            0,
        )
    }
    fn finish_upgrade(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let bundle_path = get_bin_path(state, &task.instance, "bundle");
        let sub = TaskRunner::as_user(state, &task)?;

        sub.run(&[&bundle_path, "exec", "bin/tootctl", "cache", "clear"])?;

        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;

        // overwrite db config to bypass pgbouncer for migrations
        let pg_config = PgConfig::from_inst(state, &instance)?;
        clear_db_lock(&pg_config)?;

        sub.check_run(
            &[
                &format!("--property=Environment=DB_HOST={}", pg_config.host),
                &format!("--property=Environment=DB_PORT={}", pg_config.port),
                &bundle_path,
                "exec",
                "rails",
                "db:migrate",
            ],
            0,
        )
    }
    fn create_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let bundle_path = get_bin_path(state, &task.instance, "bundle");
        let params = TaskParams::new(task)?;
        let username = params.req_str("username")?;
        let email = params.req_str("email")?;
        let role = params.req_str("role").unwrap_or("admin");

        let sub = TaskRunner::as_user(state, &task)?;
        let code = sub.run(&[
            &bundle_path,
            "exec",
            "bin/tootctl",
            "accounts",
            "create",
            username,
            "--confirmed",
            &format!("--role={role}"),
            &format!("--email={email}"),
        ])?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }

        let pw_re = Regex::new("(?m)^New password: ([a-zA-Z0-9]+)$").unwrap();

        Ok(serde_json::json!({
            "password": pw_re.captures(&output).and_then(|c|c.get(1)).map(|m| m.as_str().to_owned()),
            "output": output,
            "code": code,
        }))
    }
    fn promote_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let bundle_path = get_bin_path(state, &task.instance, "bundle");
        let params = TaskParams::new(task)?;
        let username = params.req_str("username")?;

        // 'admin' is common, glitch moved to 'owner'
        let role = params.req_str("role").unwrap_or("admin");

        let sub = TaskRunner::as_user(state, &task)?;
        let code = sub.run(&[
            &bundle_path,
            "exec",
            "bin/tootctl",
            "accounts",
            "modify",
            &username,
            &format!("--role={role}"),
            // validate email address, ensuring the acct won't be stuck with an
            // invalid address or broken SMTP config
            "--confirm",
            // approve accounts if they require registration approval,
            // so we can add the first admin even without open registrations
            "--approve"
        ])?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }

        Ok(serde_json::json!({
            "output": output,
            "code": code,
        }))
    }
    fn export(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;

        let config = state
            .startup_config
            .export
            .clone()
            .context("export not available")?;

        let pgp_key = instance.env.get("backup_pgp_key").filter(|v| !v.is_empty());

        let archive_name = format!("export_{}_{}", &task.instance, task.id.as_simple());
        let archive_ext = {
            let mut v = ".tar".to_owned();
            if pgp_key.is_some() {
                v += ".pgp"
            }
            v
        };

        let backup_root = format!("{}/{}", config.tmp_dir_path, archive_name);
        let pg_dump_path = format!("{backup_root}/pg_dump.sql.zstd");
        let settings_path = format!("{backup_root}/environment.txt");
        let recipient_file = format!("{backup_root}/pgp_recipient.asc");

        let s3_path = format!(
            "/fedimonster/export/{}/{}{}",
            task.instance, archive_name, archive_ext
        );

        std::fs::create_dir_all(&backup_root).context("failed to create backup directory")?;

        // put recipient in file if any
        if let Some(v) = pgp_key {
            std::fs::write(&recipient_file, v).context("failed to create gpg recipient file")?;
        }

        // save settings
        {
            let ctx = EnvBuilder::build(state, &task.instance, env::build)?;
            let export_vars = &[
                // instance secrets
                "OTP_SECRET",
                "PAPERCLIP_SECRET",
                "SECRET_KEY_BASE",
                "VAPID_PRIVATE_KEY",
                "VAPID_PUBLIC_KEY",
                // all the passthru settings
                "SINGLE_USER_MODE",
                "AUTHORIZED_FETCH",
                "DEFAULT_LOCALE",
                "LIMITED_FEDERATION_MODE",
                "MAX_TOOT_CHARS",
                "MAX_PINNED_TOOTS",
                "MAX_BIO_CHARS",
                "MAX_PROFILE_FIELDS",
                "MAX_DISPLAY_NAME_CHARS",
                "MAX_POLL_OPTIONS",
                "MAX_POLL_OPTION_CHARS",
                "MAX_IMAGE_SIZE",
                "MAX_VIDEO_SIZE",
                "MAX_SEARCH_RESULTS",
                "MAX_EMOJI_SIZE",
                "MAX_REMOTE_EMOJI_SIZE",
                "HCAPTCHA_SECRET_KEY",
                "HCAPTCHA_SITE_KEY",
            ];

            let filtered = ctx
                .env
                .iter()
                .filter(|(k, _)| export_vars.contains(&k.as_str()))
                .map(|(k, v)| (k.clone(), v.clone()))
                .collect();

            std::fs::write(&settings_path, serialize_env(&filtered))
                .context("failed to write environment")?;
        }

        let pg_config = PgConfig::from_inst(state, &instance)?;

        /*
        tar -> (gnupg as subprocess ->) s3 bucket
          <- env files (generated here)
          <- zstd <- pg_dump (both as subprocesses)
        */

        log::info!("starting pg dump");

        let mut pgdump_child = Command::new(get_bin_path(state, &task.instance, "pg_dump"))
            .arg(&pg_config.dbname)
            .env("PGDATABASE", &pg_config.dbname)
            .env("PGHOST", &pg_config.host)
            .env("PGPORT", &pg_config.port.to_string())
            .env("PGUSER", &pg_config.username)
            .env("PGPASSWORD", &pg_config.password)
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::null())
            .spawn()
            .context("failed to start pgdump")?;

        let mut zstd_child = Command::new(get_bin_path(state, &task.instance, "zstd"))
            .arg("-13")
            .arg("--adapt")
            .arg("-")
            .stdout(std::process::Stdio::piped())
            .stdin(pgdump_child.stdout.take().unwrap())
            .spawn()
            .context("failed to spawn zstd")?;

        log::debug!("saving pg dump to {pg_dump_path}");
        let mut dump_file = std::fs::File::create(&pg_dump_path)?;
        std::io::copy(&mut zstd_child.stdout.take().unwrap(), &mut dump_file)
            .context("copying pg dump to file")?;

        let pgdump_status = pgdump_child.wait().context("pg_dump")?;
        let compres_status = zstd_child.wait().context("zstd")?;

        log::debug!("dump finished ({}/{})", pgdump_status, compres_status);

        log::info!("building export archive");

        // so the thing is, tar needs to know the size in headers, so no streaming.
        // we have to save the sql dump first, know its size, and build the archive.
        // or seek over s3 - which exists as a crate but i dont think i could keep
        // encryption? anyway i can find a spare 50 gibytes or whatever

        let low_size = std::fs::metadata(&settings_path)
            .map(|m| m.len())
            .unwrap_or(0)
            + std::fs::metadata(&pg_dump_path)
                .map(|m| m.len())
                .unwrap_or(0);

        // build a tar, pipe it to gpg, and forward the result over s3

        log::debug!("starting tar");
        let mut tar_child = Command::new(get_bin_path(state, &task.instance, "tar"))
            .arg("cv")
            .arg("-C")
            .arg(&backup_root)
            .arg(&settings_path)
            .arg(&pg_dump_path)
            .stdout(std::process::Stdio::piped())
            .spawn()
            .context("failed to spawn tar")?;

        let output_stream = if pgp_key.is_some() {
            log::debug!("starting gpg");
            let mut gpg_child = Command::new(get_bin_path(state, &task.instance, "gpg"))
                .arg("--encrypt")
                .arg("--recipient-file")
                .arg(recipient_file)
                .arg("--no-keyring")
                .stdin(tar_child.stdout.take().unwrap())
                .stdout(std::process::Stdio::piped())
                .spawn()
                .context("failed to spawn gpg")?;
            gpg_child.stdout.take().unwrap()
        } else {
            tar_child.stdout.take().unwrap()
        };

        // open bucket and start streaming to s3

        let bucket = {
            let credentials = s3::creds::Credentials::new(
                Some(config.key_id.as_str()),
                Some(config.secret.as_str()),
                None,
                None,
                None,
            )?;
            let region = s3::Region::Custom {
                region: config.region.clone(),
                endpoint: config.host.clone(),
            };
            match s3::Bucket::create_with_path_style_blocking(
                &config.bucket_name,
                region.clone(),
                credentials.clone(),
                s3::BucketConfiguration::private(),
            ) {
                Ok(r) => log::debug!("bucket creation: {} {:?}", r.response_code, r.response_text),
                Err(e) => log::debug!("bucket creation failed ({e})"),
            }
            s3::Bucket::new(&config.bucket_name, region, credentials)?.with_path_style()
        };

        let mut output_stream = tokio::process::ChildStdout::from_std(output_stream)?;
        log::debug!("put to {s3_path} at {}/{}", config.host, config.bucket_name);
        let _status_code = bucket
            .put_object_stream_blocking(&mut output_stream, &s3_path)
            .context("failed to push export archive to s3")?;

        // ensure tar finished
        tar_child.wait().context("tar")?;

        // generate share link to the archive

        // Add optional custom queries
        let mut custom_queries = std::collections::HashMap::new();
        custom_queries.insert(
            "response-content-disposition".into(),
            format!("attachment; filename=\"{archive_name}{archive_ext}\""),
        );

        let url = bucket
            .presign_get(&s3_path, config.default_expiry, Some(custom_queries))
            .unwrap();

        // remove tmp directory
        if let Err(e) = std::fs::remove_dir_all(&backup_root) {
            log::warn!("failed to cleanup {backup_root}: {e}");
        }

        Ok(serde_json::json!({
            "export_url": url,
            "export_expiration": config.default_expiry,
            "export_size": low_size,
        }))
    }
    fn prune_caches(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;

        let days = instance
            .env
            .get("media_cache_days")
            .and_then(|s| s.parse().ok())
            .unwrap_or(state.startup_config.system.media_cache_days);
        let days = format!("--days={days}");

        let media = prune_wrapper(
            state,
            task,
            &["media", "remove", "--concurrency=3", &days],
            r"(?m)^Removed ([0-9]+) media attachments \(approx. ([^)]+)\)$",
        )?;
        let profiles = prune_wrapper(
            state,
            task,
            &["media", "remove", "--prune-profiles", &days],
            r"(?m)^Visited ([0-9]+) accounts and removed profile media totaling ([^)]+)$",
        )?;
        let preview_cards = prune_wrapper(
            state,
            task,
            &["preview_cards", "remove", "--concurrency=3", &days],
            r"(?m)^Removed ([0-9]+) preview cards \(approx. ([^)]+)\)$",
        )?;

        let deep = check_bool(task.parameters.get("deep"));
        log::error!("DEEP = {:?} = {:?}", task.parameters.get("deep"), deep);

        let orphans = if deep {
            prune_wrapper(
                state,
                task,
                &["media", "remove-orphans"],
                r"(?m)^Removed ([0-9]+) orphans \(approx. ([^)]+)\)$",
            )?
        } else {
            (None, None)
        };

        let statuses_pruned = if deep {
            // 'deep' prune mode will go thru the local database for posts to remove.
            // it is pretty resource intensive and will require calling pg_repack afterwards
            // to get any actual disk space free'd - but it should still let pg reuse the
            // discarded rows

            let bundle_path = get_bin_path(state, &task.instance, "bundle");
            let sub = TaskRunner::as_user(state, &task)?;

            let days = sub
                .inst
                .env
                .get("status_cache_days")
                .and_then(|s| s.parse().ok())
                .unwrap_or(state.startup_config.system.status_cache_days);

            let code = sub.run(&[
                &bundle_path,
                "exec",
                "env",
                "DEFAULT_LOCALE=en",
                "bin/tootctl",
                "status",
                "remove",
                &format!("--days={days}"),
                "--clean-followed",
            ])?;

            code == 0
        } else {
            false
        };

        Ok(serde_json::json!({
            "media_pruned_count": media.0,
            "media_pruned_size": media.1,
            "preview_cards_pruned_count": preview_cards.0,
            "preview_cards_pruned_size": preview_cards.1,
            "statuses_pruned": statuses_pruned,
            "orphans_pruned_count": orphans.0,
            "orphans_pruned_size": orphans.1,
            "profiles_pruned_count": profiles.0,
            "profiles_pruned_size": profiles.1,
        }))
    }

    fn update_stats(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let inst_name = &task.instance;
        let full = task.parameters.get("full") == Some(&serde_json::Value::Bool(true));

        log::debug!("gathering instance stats for {inst_name} (full: {full:?})");

        let instance = state
            .get_instance(inst_name)
            .ok_or(anyhow!("no instance {:?} found", inst_name))?;
        let mut stats = instance.stats.unwrap_or_default();

        let ctx = EnvBuilder::build(state, &task.instance, env::build)?;
        let env = ctx.env;

        // first, fast metrics (redis)

        let redis_host = env.get("REDIS_HOST").ok_or(anyhow!("missing redis host"))?;
        let redis_port = env.get("REDIS_PORT").ok_or(anyhow!("missing redis port"))?;
        let redis_password = env.get("REDIS_PASSWORD").cloned().unwrap_or_default();
        let redis_ns = env
            .get("REDIS_NAMESPACE")
            .ok_or(anyhow!("missing redis ns"))?;

        let client = redis::Client::open(format!(
            "redis://:{}@{}:{}/0",
            redis_password, redis_host, redis_port
        ))?;
        let mut con = client.get_connection().context("redis conn failed")?;

        // sidekiq
        let queues: Vec<String> = con.smembers(format!("{}:queues", redis_ns))?;
        log::trace!("found queues: {:?}", queues);
        let mut pending = 0;
        for q in queues {
            let q_size: i32 = con.llen(format!("{}:queue:{}", redis_ns, q))?;
            log::trace!("found {} pending in queue {}", q_size, q);
            pending += q_size;
        }
        stats.pending_tasks = pending as usize;

        // build keys for the 30 last 1-day periods by UTC timestamp at 0000Z
        let current_period = chrono::Utc::now().date();
        let relevant_keys: Vec<String> = (0..30)
            .map(|i| current_period - Duration::days(i))
            .map(|d| d.and_hms(0, 0, 0))
            .map(|d| d.timestamp())
            .map(|t| format!("{}:activity:logins:{}", redis_ns, t))
            .collect();

        stats.active_users = con
            .pfcount(relevant_keys)
            .context("fetching active users (redis)")?;

        // postgres stuff

        let cs = format!(
            "host={} user={} port={} dbname={} password='{}'",
            env.get("DB_HOST").ok_or(anyhow!("missing db host"))?,
            env.get("DB_USER").ok_or(anyhow!("missing db user"))?,
            env.get("DB_PORT").ok_or(anyhow!("missing db port"))?,
            env.get("DB_NAME").ok_or(anyhow!("missing db name"))?,
            env.get("DB_PASS").map(String::as_str).unwrap_or("")
        );
        let mut client =
            postgres::Client::connect(&cs, postgres::NoTls).context("pg conn failed")?;

        if full {
            stats.db_size_mb = client
                .query_one("SELECT pg_database_size(current_database());", &[])
                .and_then(|row| row.try_get::<_, i64>(0))
                .context("fetching db size")? as usize
                / 1_000_000;

            stats.media_size_mb = client.query_one("SELECT 
            ((coalesce((select sum(image_file_size) from custom_emojis), 0)
            + coalesce((select sum(COALESCE(image_file_size, 0)) from preview_cards), 0)
            + coalesce((select sum(COALESCE(avatar_file_size, 0)) + sum(header_file_size) from accounts), 0)
            + coalesce((SELECT SUM(COALESCE(file_file_size, 0) + COALESCE(thumbnail_file_size, 0)) FROM media_attachments), 0)
            + coalesce((select sum(COALESCE(dump_file_size, 0)) from backups), 0)
            + coalesce((select sum(COALESCE(data_file_size, 0)) from imports), 0)
            + coalesce((select sum(COALESCE(file_file_size, 0)) from site_uploads), 0)
            )/ 1000000)::bigint
        ;", &[])
            .and_then(|row| row.try_get::<_, i64>(0))
            .context("fetching attachment stats")? as usize;

            let local_media_size_mb = client.query_one("SELECT 
            ((coalesce((select sum(image_file_size) from custom_emojis where domain is null), 0)
            + coalesce((select sum(COALESCE(avatar_file_size, 0)) + sum(header_file_size) from accounts where domain is null), 0)
            + coalesce((SELECT SUM(COALESCE(file_file_size, 0) + COALESCE(thumbnail_file_size, 0)) FROM media_attachments where remote_url = ''), 0)
            + coalesce((select sum(COALESCE(dump_file_size, 0)) from backups), 0)
            + coalesce((select sum(COALESCE(data_file_size, 0)) from imports), 0)
            + coalesce((select sum(COALESCE(file_file_size, 0)) from site_uploads), 0)
            )/ 1000000)::bigint
        ;", &[])
            .and_then(|row| row.try_get::<_, i64>(0))
            .context("fetching attachment stats (local media)")? as usize;

            stats.media_cache_ratio = 1.0 - local_media_size_mb as f32 / stats.media_size_mb as f32;

            stats.total_users = client
                .query_one(
                    "SELECT count(*) FROM users WHERE disabled=false and approved;",
                    &[],
                )
                .and_then(|row| row.try_get::<_, i64>(0))
                .context("fetching total users")? as usize;
        }

        // save
        log::debug!("stats for {inst_name}: {stats:?}");
        state.mut_instance(&inst_name, |i| {
            i.stats = Some(stats);
            i.locally_updated = true;
        });

        Ok(serde_json::json!({}))
    }
}
