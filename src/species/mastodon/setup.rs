use crate::config::WorkerModel;
use crate::env_builder::EnvBuilder;
use crate::land::pg::PgConfig;
use crate::land::*;
use crate::land::{get_bin_path, serialize_env};
use crate::state::AppState;
use crate::task::def::TaskResult;
use crate::task::{Task, TaskError};
use anyhow::{anyhow, Context};
use std::path::PathBuf;
use std::sync::Arc;

pub const SERVICES: &[&str] = &["sidekiq", "web", "streaming"];

pub const SERVICE_ATTRIBUTES: &str = "
# Proc filesystem
ProcSubset=pid
ProtectProc=invisible
# Capabilities
CapabilityBoundingSet=
# Security
NoNewPrivileges=true
# Sandboxing
ProtectSystem=full
ReadWritePaths=/tmp
PrivateTmp=true
PrivateDevices=true
PrivateUsers=true
ProtectHostname=true
ProtectKernelLogs=true
ProtectKernelModules=true
ProtectKernelTunables=true
ProtectControlGroups=true
RestrictAddressFamilies=AF_INET
RestrictAddressFamilies=AF_INET6
RestrictAddressFamilies=AF_NETLINK
RestrictAddressFamilies=AF_UNIX
RestrictNamespaces=true
LockPersonality=true
RestrictRealtime=true
RestrictSUIDSGID=true
RemoveIPC=true
PrivateMounts=true
ProtectClock=true
# System Call Filtering
SystemCallArchitectures=native
SystemCallFilter=~@cpu-emulation @debug @keyring @ipc @mount @obsolete @privileged @setuid
SystemCallFilter=@chown
SystemCallFilter=pipe
SystemCallFilter=pipe2";

pub fn instance_setup<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();

    let ctx = EnvBuilder::build(state, &task.instance, super::env::build)?;

    let inst_name = &task.instance;
    let inst = state.get_instance(inst_name).unwrap();
    let user = ctx.env.get("TROUPO_USER").unwrap();
    let dir = ctx.env.get("TROUPO_APP_DIR").unwrap();

    if !std::path::Path::new(dir).exists() {
        return Err(anyhow!("mastodon installation not found").into());
    }

    let env_path = PathBuf::from(format!("/etc/troupo/env/{inst_name}"));
    changes.push(
        T::set_text(&env_path, &serialize_env(&ctx.env)).context("writing instance env file")?,
        &format!("/etc/troupo/env/{inst_name}"),
    );

    let web_scale = &ctx.instance_config.web_scale;
    let worker_scale = &ctx.instance_config.worker_scale;

    // create database

    log::debug!("creating pg user and db");
    let pg_config = PgConfig::from_inst(state, &inst)?;
    crate::land::pg::setup::<T>(&pg_config, &mut changes)?;
    pgbouncer::setup::<T>(state, task, &mut changes, "transaction")?;

    // install services

    let bundle_path = get_bin_path(state, &inst_name, "bundle");
    let node_path = get_bin_path(state, &inst_name, "node");

    let mut services: Vec<_> = SERVICES.iter().collect();
    if worker_scale.model == WorkerModel::Heavy {
        services.push(&"sidekiq2");
        services.push(&"sidekiq3");
    }

    for svc_key in &services {
        let svc_path = PathBuf::from(format!(
            "/etc/systemd/system/mastodon-{inst_name}-{svc_key}.service"
        ));
        let description = match **svc_key {
            "web" => {
                let args = format!("--workers {} --threads {}:{}", web_scale.processes, web_scale.min_threads, web_scale.max_threads);
                let db_pool = web_scale.processes.min(1) * web_scale.max_threads + 5;
                ServiceBuilder::simple(svc_key)
                .with_user(&user)
                .with_dir(&dir)
                .with_env_file(&format!("/etc/troupo/env/{inst_name}"))
                .with_env("LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so")
                .with_env(&format!("DB_POOL={}", db_pool))
                .with_env(&format!("PORT={}", 20_000 + inst.local_id))
                .with_exec_start(&format!("{bundle_path} exec puma -C config/puma.rb {args}"))
                .with_exec_reload("/bin/kill -SIGUSR1 $MAINPID")
                .with_max_mem(web_scale.mem_mb)
                .with_service_attr(SERVICE_ATTRIBUTES)
                .build()
            },
            "sidekiq" => {
                let mut queues = "-q default -q mailers -q ingress -q push".to_owned();
                if inst.run_scheduler {
                    queues += " -q scheduler";
                }
                if worker_scale.model != WorkerModel::Heavy {
                    queues += " -q pull";
                }
                let args = format!("-c {} {}", worker_scale.threads, queues);
                let db_pool = worker_scale.threads + 5;
                ServiceBuilder::simple(svc_key)
                .with_user(&user)
                .with_dir(&dir)
                .with_env_file(&format!("/etc/troupo/env/{inst_name}"))
                .with_env("LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so")
                .with_env(&format!("DB_POOL={}", db_pool))
                .with_exec_start(&format!("{bundle_path} exec sidekiq {args}"))
                .with_exec_reload("/bin/kill -SIGUSR1 $MAINPID")
                .with_max_mem(worker_scale.mem_mb)
                .with_service_attr(SERVICE_ATTRIBUTES)
                .build()
            },
            "sidekiq2" => {
                let queues = "-q ingress -q push -q default -q pull".to_owned();
                let args = format!("-c {} {}", worker_scale.threads, queues);
                let db_pool = worker_scale.threads + 5;
                ServiceBuilder::simple(svc_key)
                .with_user(&user)
                .with_dir(&dir)
                .with_env_file(&format!("/etc/troupo/env/{inst_name}"))
                .with_env("LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so")
                .with_env(&format!("DB_POOL={}", db_pool))
                .with_exec_start(&format!("{bundle_path} exec sidekiq {args}"))
                .with_exec_reload("/bin/kill -SIGUSR1 $MAINPID")
                .with_max_mem(worker_scale.mem_mb)
                .with_service_attr(SERVICE_ATTRIBUTES)
                .build()
            },
            "sidekiq3" => {
                let queues = "-q push -q ingress -q default -q pull".to_owned();
                let args = format!("-c {} {}", worker_scale.threads, queues);
                let db_pool = worker_scale.threads + 5;
                ServiceBuilder::simple(svc_key)
                .with_user(&user)
                .with_dir(&dir)
                .with_env_file(&format!("/etc/troupo/env/{inst_name}"))
                .with_env("LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so")
                .with_env(&format!("DB_POOL={}", db_pool))
                .with_exec_start(&format!("{bundle_path} exec sidekiq {args}"))
                .with_exec_reload("/bin/kill -SIGUSR1 $MAINPID")
                .with_max_mem(worker_scale.mem_mb)
                .with_service_attr(SERVICE_ATTRIBUTES)
                .build()
            },
            "streaming" => {
                ServiceBuilder::simple(svc_key)
                .with_user(&user)
                .with_dir(&dir)
                .with_env_file(&format!("/etc/troupo/env/{inst_name}"))
                .with_env(&format!("PORT={}", 30_000 + inst.local_id))
                .with_exec_start(&format!("{node_path} ./streaming"))
                .with_exec_reload("/bin/kill -SIGUSR1 $MAINPID")
                .with_max_mem(worker_scale.mem_mb)
                .with_service_attr(SERVICE_ATTRIBUTES)
                .build()
            },
            _ => panic!("matching a service that does not exist. this should never happen since they're specified right over here")
        };

        changes.push(
            T::set_text(&svc_path, &description)
                .with_context(|| format!("writing svc {svc_key} for inst {inst_name}"))?,
            svc_key,
        );
    }

    // start & enable services

    T::run_as_root(state, task, &["systemctl".into(), "daemon-reload".into()])?;

    for svc_key in &services {
        let svc = format!("mastodon-{inst_name}-{svc_key}.service");
        if T::svc_is_enabled(state, task, &svc)? {
            T::run_as_root(state, task, &["systemctl", "restart", &svc])?;
        } else {
            T::run_as_root(state, task, &["systemctl", "enable", "--now", &svc])?;
        }
    }

    Ok(changes)
}

pub fn instance_remove<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();

    let ctx = EnvBuilder::build(state, &task.instance, super::env::build)?;

    let inst_name = &task.instance;

    // clean up services
    for svc_key in SERVICES.iter() {
        let svc = format!("mastodon-{inst_name}-{svc_key}.service");
        let svc_path = PathBuf::from(format!(
            "/etc/systemd/system/mastodon-{inst_name}-{svc_key}.service"
        ));

        T::run_as_root(state, task, &["systemctl", "disable", "--now", &svc])?;

        changes.push(T::delete(&svc_path)?, &format!("delete service {svc_key}"));
    }

    // clean up env
    changes.push(
        T::delete(&PathBuf::from(format!("/etc/troupo/env/{inst_name}")))?,
        "delete env file",
    );

    // update pgbouncer
    pgbouncer::setup::<T>(state, task, &mut changes, "transaction")?;

    Ok(changes)
}
