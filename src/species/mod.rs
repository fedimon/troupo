use crate::config::Species;
use crate::state::AppState;
use crate::task::{Task, TaskError, TaskResult, TaskType};
use anyhow::anyhow;
use std::sync::Arc;

fn unimpl() -> TaskResult {
    Err(TaskError::from(anyhow!(
        "task not implemented for this species!"
    )))
}

pub trait Animal {
    fn deploy(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn remove(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn check(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn restart(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn command(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn initialize(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn prepare_upgrade(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn finish_upgrade(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn create_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn promote_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn export(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn prune_caches(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn update_stats(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
}

pub mod gotosocial;
pub mod mastodon;
pub mod pixelfed;
pub mod redis;

pub fn dispatch_task<T: Animal>(state: &Arc<AppState>, task: &Task) -> TaskResult {
    match task.task_type {
        TaskType::Deploy => T::deploy(&state, &task),
        TaskType::Remove => T::remove(&state, &task),
        TaskType::Check => T::check(&state, &task),
        TaskType::Restart => T::restart(&state, &task),
        TaskType::Command => T::command(&state, &task),
        TaskType::Initialize => T::initialize(&state, &task),
        TaskType::PrepareUpgrade => T::prepare_upgrade(&state, &task),
        TaskType::FinishUpgrade => T::finish_upgrade(&state, &task),
        TaskType::CreateUser => T::create_user(&state, &task),
        TaskType::PromoteUser => T::promote_user(&state, &task),
        TaskType::Export => T::export(&state, &task),
        TaskType::PruneCaches => T::prune_caches(&state, &task),
        TaskType::UpdateStats => T::update_stats(&state, &task),
    }
}

pub fn dispatch_species(state: &Arc<AppState>, species: Species, task: &Task) -> TaskResult {
    match species {
        Species::Mastodon => dispatch_task::<mastodon::Mastodon>(state, task),
        Species::Pixelfed => dispatch_task::<pixelfed::Pixelfed>(state, task),
        Species::GoToSocial => dispatch_task::<gotosocial::GoToSocial>(state, task),
    }
}
