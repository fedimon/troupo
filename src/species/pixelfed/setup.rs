use crate::env_builder::EnvBuilder;
use crate::land::pg::PgConfig;
use crate::land::*;
use crate::land::{get_bin_path, serialize_env};
use crate::state::AppState;
use crate::task::def::TaskResult;
use crate::task::Task;
use anyhow::{anyhow, Context};
use std::path::PathBuf;
use std::sync::Arc;

fn build_fpm_config(ctx: &EnvBuilder, username: &str) -> String {
    let root_dir = format!("/home/{username}/");
    let mut s = format!(
        "
[{username}]
listen = /var/run/php-fpm.{username}.sock
listen.owner = www-data
listen.group = www-data
listen.mode = 0750
user = {username}
group = {username}

pm = ondemand
pm.max_children = 4
pm.max_requests = 500

php_admin_value[memory_limit] = 768M
php_admin_value[open_basedir] = {root_dir}:/tmp
php_admin_value[post_max_size] = 1G 
php_admin_value[upload_max_filesize] = 1G
php_admin_value[max_file_uploads] = 100
php_admin_value[max_execution_time] = 600

"
    );

    for (key, value) in ctx.env.iter() {
        // TODO quoting lol
        if value.is_empty() {
            // this will just make php-fpm refuse the configuration, quoted or not
            continue;
        }
        s.push_str(&format!("env[{key}] = \"{value}\"\n"));
    }

    s
}

fn build_nginx_config(inst_name: &str, username: &str, domain: &str) -> String {
    format!(
        r#"
server {{
    listen 80;
    listen [::]:80;
    server_name {domain};
    root /home/{username}/pixelfed/public;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.html index.htm index.php;

    charset utf-8;
    client_max_body_size 512M;

    location / {{
        try_files $uri $uri/ /index.php?$query_string;
    }}

    location = /favicon.ico {{ access_log off; log_not_found off; }}
    location = /robots.txt  {{ access_log off; log_not_found off; }}

    error_page 404 /index.php;

    location ~ \.php$ {{
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php-fpm.{username}.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name; # or $request_filename
    }}

    location ~ /\.(?!well-known).* {{
        deny all;
    }}
}}

"#
    )
}

fn build_redis_conf(username: &str) -> String {
    format!(
        r#"
unixsocket /home/{username}/redis/redis.sock
bind 127.0.0.1
port 0
timeout 0
pidfile /home/{username}/redis/redis-server.pid
loglevel notice
syslog-enabled yes
syslog-ident redis
databases 3
always-show-logo no
save 900 1
save 300 10
save 60 10000
protected-mode no
stop-writes-on-bgsave-error yes
rdbcompression yes
rdbchecksum yes
dbfilename dump.rdb
rdb-del-sync-files no
dir /home/{username}/redis/
replica-serve-stale-data yes
replica-read-only yes
repl-diskless-sync no
repl-diskless-sync-delay 5
repl-diskless-load disabled
repl-disable-tcp-nodelay no
replica-priority 100
acllog-max-len 128
lazyfree-lazy-eviction no
lazyfree-lazy-expire no
lazyfree-lazy-server-del no
replica-lazy-flush no
lazyfree-lazy-user-del no
oom-score-adj no
oom-score-adj-values 0 200 800
appendonly no
appendfilename "appendonly.aof"
appendfsync everysec
no-appendfsync-on-rewrite no
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated yes
aof-use-rdb-preamble yes
lua-time-limit 5000
slowlog-log-slower-than 10000
slowlog-max-len 128
latency-monitor-threshold 0
notify-keyspace-events ""
hash-max-ziplist-entries 512
hash-max-ziplist-value 64
list-max-ziplist-size -2
list-compress-depth 0
set-max-intset-entries 512
zset-max-ziplist-entries 128
zset-max-ziplist-value 64
hll-sparse-max-bytes 3000
stream-node-max-bytes 4096
stream-node-max-entries 100
activerehashing yes
client-output-buffer-limit normal 0 0 0
client-output-buffer-limit replica 256mb 64mb 60
client-output-buffer-limit pubsub 32mb 8mb 60
hz 10
dynamic-hz yes
aof-rewrite-incremental-fsync yes
rdb-save-incremental-fsync yes
jemalloc-bg-thread yes
"#
    )
}

fn build_redis_unit(username: &str) -> String {
    format!(
        r#"
[Unit]
Description=Advanced key-value store
After=network.target
Documentation=http://redis.io/documentation, man:redis-server(1)

[Service]
Type=notify
ExecStart=/usr/bin/redis-server /home/{username}/redis/redis.conf --supervised systemd --daemonize no
PIDFile=/home/{username}/redis/redis-server.pid
TimeoutStopSec=0
Restart=always
User={username}
Group={username}
RuntimeDirectory=redis
RuntimeDirectoryMode=2755

UMask=007
PrivateTmp=yes
LimitNOFILE=65535
PrivateDevices=yes
ReadOnlyDirectories=/
ReadWritePaths=-/home/{username}/redis/

NoNewPrivileges=true
CapabilityBoundingSet=CAP_SETGID CAP_SETUID CAP_SYS_RESOURCE
MemoryDenyWriteExecute=true
ProtectKernelModules=true
ProtectKernelTunables=true
ProtectControlGroups=true
RestrictRealtime=true
RestrictNamespaces=true
RestrictAddressFamilies=AF_UNIX

# redis-server can write to its own config file when in cluster mode so we
# permit writing there by default. If you are not using this feature, it is
# recommended that you replace the following lines with "ProtectSystem=full".
ProtectSystem=full

[Install]
WantedBy=multi-user.target
"#
    )
}

pub fn save_env<T: Actor>(state: &Arc<AppState>, inst_name: &str) -> TaskResult<Changes> {
    let ctx = EnvBuilder::build(state, inst_name, super::env::build)?;
    let env_path = PathBuf::from(format!("/etc/troupo/env/{inst_name}"));
    Ok(T::set_text(&env_path, &serialize_env(&ctx.env)).context("writing instance env file")?)
}

pub fn instance_setup<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();

    changes.push(
        save_env::<T>(state, &task.instance)?,
        &format!("/etc/troupo/env/{}", task.instance),
    );

    let ctx = EnvBuilder::build(state, &task.instance, super::env::build)?;

    // build environment file
    let inst_name = &task.instance;
    let env_path = PathBuf::from(format!("/etc/troupo/env/{inst_name}"));
    changes.push(
        T::set_text(&env_path, &serialize_env(&ctx.env)).context("writing instance env file")?,
        &format!("/etc/troupo/env/{inst_name}"),
    );

    // create database
    log::debug!("creating database user");
    let pg_config = PgConfig::from_inst(state, &ctx.instance_config)?;
    crate::land::pg::setup::<T>(&pg_config, &mut changes)?;
    pgbouncer::setup::<T>(state, task, &mut changes, "session")?;

    // create local user
    let username = crate::land::runner::get_username(inst_name, &ctx.instance_config);
    log::debug!("creating instance user ({username})");
    changes.push(
        crate::land::system::setup_user::<T>(state, task, &username)?,
        "user",
    );

    // clone to tag
    log::debug!("cloning repository");
    let inst_dir = format!("/home/{username}/pixelfed/");
    let branch = state
        .startup_config
        .versions
        .pixelfed
        .get(&ctx.instance_config.version)
        .ok_or(anyhow!("version not found locally"))?;
    let repo_url = "https://github.com/pixelfed/pixelfed.git";
    let repo_path = PathBuf::from(&inst_dir);
    T::mkdir(&repo_path)?;
    T::run_as_root(
        state,
        task,
        &["chown", &format!("{username}:{username}"), "-R", &inst_dir],
    )?;
    let git_path = {
        let mut p = repo_path.clone();
        p.push(".git");
        p
    };
    if git_path.is_dir() {
        T::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &["git", "fetch", "--tags", "origin", &branch],
        )?;
        T::run_as_inst_in(state, task, &inst_dir, &["git", "checkout", "-d", &branch])?;
    } else {
        T::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &[
                "git",
                "clone",
                repo_url,
                ".",
                "--depth=1",
                "--branch",
                &branch,
            ],
        )?;
    }

    // setup php-fpm
    let fpm_version = "8.2";
    // TODO: use a config value for this
    log::debug!("setting up php-fpm conf (version {fpm_version})");
    let conf_path = PathBuf::from(format!(
        "/etc/php/{fpm_version}/fpm/pool.d/{inst_name}.conf"
    ));
    changes.push(
        T::set_text(&conf_path, &build_fpm_config(&ctx, &username))
            .context("writing fpm config file")?,
        &format!("writing {conf_path:?}"),
    );
    T::run_as_root(
        state,
        task,
        &[
            "systemctl",
            "reload",
            &format!("php{fpm_version}-fpm.service"),
        ],
    )?;

    // redis conf
    log::debug!("setting up dedicated redis");
    T::mkdir(&PathBuf::from(format!("/home/{username}/redis/")))?;
    T::run_as_root(
        state,
        task,
        &[
            "chown",
            &format!("{username}:{username}"),
            "-R",
            &format!("/home/{username}/redis/"),
        ],
    )?;
    let redis_conf_path = PathBuf::from(format!("/home/{username}/redis/redis.conf"));
    let redis_conf = build_redis_conf(&username);
    changes.push(
        T::set_text(&redis_conf_path, &redis_conf)
            .with_context(|| format!("writing redis conf for inst {inst_name}"))?,
        &format!("{redis_conf_path:?}"),
    );

    // setup dedicated redis
    let redis_svc_path = PathBuf::from(format!(
        "/etc/systemd/system/pixelfed-{inst_name}-redis.service"
    ));
    let redis_unit = build_redis_unit(&username);
    changes.push(
        T::set_text(&redis_svc_path, &redis_unit)
            .with_context(|| format!("writing redis svc for inst {inst_name}"))?,
        &format!("pixelfed-{inst_name}-redis.service"),
    );

    log::debug!("installing pixelfed services");

    // setup worker service
    let worker_svc_path = PathBuf::from(format!(
        "/etc/systemd/system/pixelfed-{inst_name}-worker.service"
    ));
    let worker_unit = format!(
        "
[Unit]
Description=Pixelfed task queueing via Laravel Horizon
After=network.target
Requires=php{fpm_version}-fpm.service
Requires=pixelfed-{inst_name}-redis.service

[Service]
Type=simple
EnvironmentFile=/etc/troupo/env/{inst_name}
ExecStart=/usr/bin/php /home/{username}/pixelfed/artisan horizon
User={username}
Restart=on-failure
MemoryMax=1G

[Install]
WantedBy=multi-user.target
"
    );
    changes.push(
        T::set_text(&worker_svc_path, &worker_unit)
            .with_context(|| format!("writing worker svc for inst {inst_name}"))?,
        &format!("pixelfed-{inst_name}-worker.service"),
    );

    // setup scheduler timer service
    let sched_timer = format!(
        "
[Unit]
Description=Pixelfed task scheduler timer

[Timer]
OnBootSec=5min
OnUnitActiveSec=1min

[Install]
WantedBy=timers.target
"
    );
    let sched_service = format!(
        "
[Unit]
Description=Pixelfed task scheduler
After=network.target
Requires=pixelfed-{inst_name}-redis.service

[Service]
Type=oneshot
EnvironmentFile=/etc/troupo/env/{inst_name}
ExecStart=/usr/bin/php /home/{username}/pixelfed/artisan schedule:run
User={username}
MemoryMax=1G

[Install]
WantedBy=multi-user.target
"
    );
    let sched_base = format!("/etc/systemd/system/pixelfed-{inst_name}-scheduler");
    changes.push(
        T::set_text(&PathBuf::from(format!("{sched_base}.timer")), &sched_timer)
            .with_context(|| format!("writing scheduler timer for inst {inst_name}"))?,
        &format!("{sched_base}.timer"),
    );
    changes.push(
        T::set_text(
            &PathBuf::from(format!("{sched_base}.service")),
            &sched_service,
        )
        .with_context(|| format!("writing scheduler service for inst {inst_name}"))?,
        &format!("{sched_base}.service"),
    );

    // setup nginx local proxy
    log::debug!("setting up local nginx->php-fpm proxy");
    let nginx_conf_path = PathBuf::from(format!("/etc/nginx/conf.d/inst_{inst_name}.conf"));
    changes.push(
        T::set_text(
            &nginx_conf_path,
            &build_nginx_config(&inst_name, &username, &ctx.instance_config.domain),
        )
        .context("writing nginx config file")?,
        &format!("writing {nginx_conf_path:?}"),
    );
    T::run_as_root(
        state,
        task,
        &["systemctl".into(), "reload".into(), "nginx".into()],
    )?;

    log::debug!("running inner install scripts");
    // run all local installation commands
    // and switch the config cache off for the install
    // i assume it runs config:cache but i dont want this step to fail.
    // we also save the state so if it was disabled, we keep it disabled
    let mut initial_cache_status = None;
    state.mut_instance(&task.instance, |i| {
        initial_cache_status = i.env.insert("ENABLE_CONFIG_CACHE".into(), "false".into());
    });
    changes.push(
        save_env::<T>(state, &task.instance)?,
        &format!("/etc/troupo/env/{}", task.instance),
    );
    T::run_as_inst_in(
        state,
        task,
        &inst_dir,
        &[
            "composer".into(),
            "install".into(),
            "--no-ansi".into(),
            "--no-interaction".into(),
            "--optimize-autoloader".into(),
        ],
    )?;
    state.mut_instance(&task.instance, |i| {
        i.env.insert(
            "ENABLE_CONFIG_CACHE".into(),
            initial_cache_status.unwrap_or("true".into()),
        );
    });
    changes.push(
        save_env::<T>(state, &task.instance)?,
        &format!("/etc/troupo/env/{}", task.instance),
    );

    log::debug!("updating horizon public files");
    T::run_as_inst_in(
        state,
        task,
        &inst_dir,
        &["php".into(), "artisan".into(), "horizon:publish".into()],
    )?;

    // those two should be logged for errors, but arent critical and will
    // fail on first run, before initial setup
    log::debug!("re-building cache");
    T::run_as_inst_in(
        state,
        task,
        &inst_dir,
        &["php".into(), "artisan".into(), "cache:clear".into()],
    );
    T::run_as_inst_in(
        state,
        task,
        &inst_dir,
        &["php".into(), "artisan".into(), "config:cache".into()],
    );
    T::run_as_inst_in(
        state,
        task,
        &inst_dir,
        &["php".into(), "artisan".into(), "route:cache".into()],
    );
    T::run_as_inst_in(
        state,
        task,
        &inst_dir,
        &["php".into(), "artisan".into(), "view:cache".into()],
    );

    // enable all those services
    log::debug!("restarting services");
    T::run_as_root(state, task, &["systemctl".into(), "daemon-reload".into()])?;
    for svc in [
        format!("pixelfed-{inst_name}-redis.service"),
        format!("pixelfed-{inst_name}-worker.service"),
        format!("pixelfed-{inst_name}-scheduler.service"),
        format!("pixelfed-{inst_name}-scheduler.timer"),
    ] {
        if T::svc_is_enabled(state, task, &svc)? {
            T::run_as_root(state, task, &["systemctl", "restart", &svc])?;
        } else {
            T::run_as_root(state, task, &["systemctl", "enable", "--now", &svc])?;
        }
    }

    Ok(changes)
}

pub fn instance_remove<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();
    let inst_name = &task.instance;

    // clean up services
    for svc in [
        format!("pixelfed-{inst_name}-redis.service"),
        format!("pixelfed-{inst_name}-worker.service"),
        format!("pixelfed-{inst_name}-scheduler.service"),
        format!("pixelfed-{inst_name}-scheduler.timer"),
    ] {
        let svc_path = PathBuf::from(format!(
            "/etc/systemd/system/pixelfed-{inst_name}-{svc}.service"
        ));

        T::run_as_root(state, task, &["systemctl", "disable", "--now", &svc])?;

        changes.push(T::delete(&svc_path)?, &format!("delete service {svc}"));
    }

    // clean up env
    changes.push(
        T::delete(&PathBuf::from(format!("/etc/troupo/env/{inst_name}")))?,
        "delete env file",
    );

    // remove php-fpm pool
    let fpm_version = "8.2";
    changes.push(
        T::delete(&PathBuf::from(format!(
            "/etc/php/{fpm_version}/fpm/pool.d/{inst_name}.conf"
        )))?,
        "delete php fpm pool file",
    );
    T::run_as_root(
        state,
        task,
        &[
            "systemctl",
            "reload",
            &format!("php{fpm_version}-fpm.service"),
        ],
    )?;

    // update pgbouncer
    pgbouncer::setup::<T>(state, task, &mut changes, "session")?;

    Ok(changes)
}
