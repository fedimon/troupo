use super::{unimpl, Animal};
use crate::env_builder::EnvBuilder;
use crate::land::get_bin_path;
use crate::land::{Actor, LocalCheckActor, LocalWriteActor, TaskRunner};
use crate::state::AppState;
use crate::task::{Task, TaskError, TaskParams, TaskResult};
use anyhow::{anyhow, Context};
use std::sync::Arc;

pub mod env;
pub mod setup;

use setup::{instance_remove, instance_setup, save_env};

pub struct Pixelfed {}
impl Animal for Pixelfed {
    fn deploy(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let _check_result = Self::check(state, task)?;
        let changes = instance_setup::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn remove(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let _check_result = Self::check(state, task)?;
        let changes = instance_remove::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn check(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let changes = instance_setup::<LocalCheckActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn restart(_state: &Arc<AppState>, _task: &Task) -> TaskResult {
        Ok(serde_json::json!({}))
    }
    fn command(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let params = TaskParams::new(task)?;
        let cmd = params.req_str("cmd")?;

        let cmd = shlex::split(cmd).ok_or(anyhow!("failed to parse command input"))?;
        let cmd: Vec<&str> = cmd.iter().map(String::as_str).collect();

        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;
        let username = crate::land::runner::get_username(&task.instance, &instance);
        let inst_dir = format!("/home/{username}/pixelfed/");

        let mut sub = TaskRunner::as_user(state, &task)?;
        let sub = sub.with_directory(&inst_dir);
        let code = sub.run(&cmd)?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }

        Ok(serde_json::json!({
            "output": output,
            "code": code,
        }))
    }
    fn initialize(state: &Arc<AppState>, task: &Task) -> TaskResult {
        // disable config cache, otherwise it might try to use it before
        // tables are even created
        state.mut_instance(&task.instance, |i| {
            i.env.insert("ENABLE_CONFIG_CACHE".into(), "false".into());
        });
        // save_env::<LocalWriteActor>(state, &task.instance)?;

        // run an initial deploy
        // ignore errors because the first deploy always fails
        let _changes = instance_setup::<LocalWriteActor>(state, task);

        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;

        let username = crate::land::runner::get_username(&task.instance, &instance);
        let inst_dir = format!("/home/{username}/pixelfed/");

        LocalWriteActor::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &[
                "php".into(),
                "artisan".into(),
                "migrate".into(),
                "--force".into(),
            ],
        )?;

        LocalWriteActor::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &["php".into(), "artisan".into(), "import:cities".into()],
        )?;

        LocalWriteActor::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &["php".into(), "artisan".into(), "instance:actor".into()],
        )?;

        LocalWriteActor::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &["php".into(), "artisan".into(), "passport:keys".into()],
        )?;

        LocalWriteActor::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &["php".into(), "artisan".into(), "storage:link".into()],
        )?;

        LocalWriteActor::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &["php".into(), "artisan".into(), "horizon:install".into()],
        )?;

        // once it succeeds, config cache can be toggled back on (default)
        state.mut_instance(&task.instance, |i| {
            i.env.insert("ENABLE_CONFIG_CACHE".into(), "true".into());
        });

        Ok(serde_json::json!({}))
    }
    fn prepare_upgrade(_state: &Arc<AppState>, _task: &Task) -> TaskResult {
        Ok(serde_json::json!({}))
    }
    fn finish_upgrade(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;

        let username = crate::land::runner::get_username(&task.instance, &instance);
        let inst_dir = format!("/home/{username}/pixelfed/");

        let output = LocalWriteActor::run_as_inst_in(
            state,
            task,
            &inst_dir,
            &[
                "php".into(),
                "artisan".into(),
                "migrate".into(),
                "--force".into(),
            ],
        )?;

        Ok(serde_json::json!({
            "output": output,
        }))
    }
    fn create_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        /*   it does not work as this command seems to ignore arguments and
             prompt anyway
        let params = TaskParams::new(task)?;
        let username = params.req_str("username")?;
        let email = params.req_str("email")?;

        let instance = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("no instance {:?} found", task.instance))?;
        let inst_username = crate::land::runner::get_username(&task.instance, &instance);
        let inst_dir = format!("/home/{inst_username}/pixelfed/");

        let mut sub = TaskRunner::as_user(state, &task)?;
        let sub = sub.with_directory(&inst_dir);
        let code = sub.run(&[
            "php".into(),
            "artisan".into(),
            "user:create".into(),
            "--confirm_email=1".into(),
            "--is_admin=1".into(),
            format!("--username={username}"),
            format!("--email={email}"),
        ])?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }

        Ok(serde_json::json!({
            "output": output,
            "code": code,
        }))
        */
        unimpl()
    }
    fn promote_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let params = TaskParams::new(task)?;
        let username = params.req_str("username")?;
        let ctx = EnvBuilder::build(state, &task.instance, env::build)?;
        let env = &ctx.env;
        let (db_user, db_pass, db_name) = ctx.get_pg_values()?;

        let cs = format!(
            "host={} user={} port={} dbname={} password='{}'",
            env.get("DB_HOST").ok_or(anyhow!("missing db host"))?,
            db_user,
            env.get("DB_PORT").ok_or(anyhow!("missing db port"))?,
            db_name,
            db_pass,
        );
        let mut client =
            postgres::Client::connect(&cs, postgres::NoTls).context("pg conn failed")?;

        let changed = client.execute(
            "UPDATE users SET is_admin=true WHERE username=$1",
            &[&username],
        )?;

        if changed == 0 {
            return Err(TaskError::from(anyhow!("user not found")));
        }

        Ok(serde_json::json!({}))
    }
    fn export(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }
    fn prune_caches(state: &Arc<AppState>, task: &Task) -> TaskResult {
        unimpl()
    }

    fn update_stats(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let inst_name = &task.instance;
        let full = task.parameters.get("full") == Some(&serde_json::Value::Bool(true));

        log::debug!("gathering instance stats for {inst_name} (full: {full:?})");

        let instance = state
            .get_instance(inst_name)
            .ok_or(anyhow!("no instance {:?} found", inst_name))?;
        let mut stats = instance.stats.unwrap_or_default();

        let ctx = EnvBuilder::build(state, &task.instance, env::build)?;
        let env = &ctx.env;

        let (db_user, db_pass, db_name) = ctx.get_pg_values()?;

        let cs = format!(
            "host={} user={} port={} dbname={} password='{}'",
            env.get("DB_HOST").ok_or(anyhow!("missing db host"))?,
            db_user,
            env.get("DB_PORT").ok_or(anyhow!("missing db port"))?,
            db_name,
            db_pass,
        );
        let mut client =
            postgres::Client::connect(&cs, postgres::NoTls).context("pg conn failed")?;

        if full {
            stats.db_size_mb = client
                .query_one("SELECT pg_database_size(current_database());", &[])
                .and_then(|row| row.try_get::<_, i64>(0))
                .context("fetching db size")? as usize
                / 1_000_000;

            stats.media_size_mb = client
                .query_one(
                    "
            SELECT ((
                COALESCE((SELECT sum(size) FROM media WHERE cdn_url IS NOT NULL), 0) +
                COALESCE((SELECT sum(size) FROM avatars WHERE cdn_url IS NOT NULL), 0)
            ) / 1000000)::bigint
        ;",
                    &[],
                )
                .and_then(|row| row.try_get::<_, i64>(0))
                .context("fetching attachment stats")? as usize;

            stats.total_users = client
                .query_one("SELECT count(*) FROM users;", &[])
                .and_then(|row| row.try_get::<_, i64>(0))
                .context("fetching total users")? as usize;
        }

        // save
        log::debug!("stats for {inst_name}: {stats:?}");
        state.mut_instance(&inst_name, |i| {
            i.stats = Some(stats);
            i.locally_updated = true;
        });

        Ok(serde_json::json!({}))
    }
}
