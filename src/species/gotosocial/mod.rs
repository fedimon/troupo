use super::{unimpl, Animal};
use crate::land::*;
use crate::state::AppState;
use crate::task::{Task, TaskError, TaskParams, TaskResult};
use anyhow::anyhow;
use std::path::PathBuf;
use std::sync::Arc;

mod config;
mod setup;

use setup::{instance_remove, instance_setup, path_push};

fn app_command(state: &Arc<AppState>, task: &Task, cmd: &[&str]) -> TaskResult<(String, usize)> {
    let mut cmd = Vec::from(cmd);

    let instance = state
        .get_instance(&task.instance)
        .ok_or(anyhow!("no instance {:?} found", task.instance))?;
    let username = crate::land::runner::get_username(&task.instance, &instance);

    let home = PathBuf::from(format!("/home/{username}/"));
    let config_path = path_push(&home, "config.json");
    let config_path_str = config_path.to_string_lossy();
    let gts_bin_path = path_push(&home, "gotosocial");
    let gts_bin_path_str = gts_bin_path.to_string_lossy().into_owned();

    cmd.insert(0, &gts_bin_path_str);
    cmd.insert(1, "--config-path");
    cmd.insert(2, &config_path_str);

    let mut sub = TaskRunner::as_user(state, &task)?;
    let sub = sub.with_directory(&format!("/home/{username}/"));
    let code = sub.run(&cmd)?;
    let output = sub.output()?;
    if code != 0 {
        return Err(TaskError::command(code, output));
    }

    Ok((output, code))
}

pub struct GoToSocial {}
impl Animal for GoToSocial {
    fn deploy(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let changes = instance_setup::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn remove(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let changes = instance_remove::<LocalWriteActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn check(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let changes = instance_setup::<LocalCheckActor>(state, task)?;
        Ok(serde_json::to_value(changes)?)
    }
    fn restart(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let inst_name = &task.instance;
        LocalWriteActor::run_as_root(
            state,
            task,
            &["systemctl", "restart", &format!("gts-{inst_name}.service")],
        )?;
        Ok(serde_json::json!({}))
    }
    fn command(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let params = TaskParams::new(task)?;
        let cmd =
            shlex::split(params.req_str("cmd")?).ok_or(anyhow!("failed to parse command input"))?;
        let cmd: Vec<&str> = cmd.iter().map(String::as_str).collect();
        let (output, code) = app_command(state, task, &cmd)?;
        Ok(serde_json::json!({
            "output": output,
            "code": code,
        }))
    }
    fn initialize(_state: &Arc<AppState>, _task: &Task) -> TaskResult {
        Ok(serde_json::json!({}))
    }
    fn prepare_upgrade(_state: &Arc<AppState>, _task: &Task) -> TaskResult {
        Ok(serde_json::json!({}))
    }
    fn finish_upgrade(_state: &Arc<AppState>, _task: &Task) -> TaskResult {
        Ok(serde_json::json!({}))
    }
    fn create_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let params = TaskParams::new(task)?;
        let (create_output, _code) = app_command(
            state,
            task,
            &[
                "admin",
                "account",
                "create",
                "--username",
                params.req_str("username")?,
                "--email",
                params.req_str("email")?,
                "--password",
                params.req_str("password")?,
            ],
        )?;

        let (confirm_output, _code) = app_command(
            state,
            task,
            &[
                "admin",
                "account",
                "confirm",
                "--username",
                params.req_str("username")?,
            ],
        )?;

        let (promote_output, code) = app_command(
            state,
            task,
            &[
                "admin",
                "account",
                "promote",
                "confirm",
                "--username",
                params.req_str("username")?,
            ],
        )?;

        Ok(serde_json::json!({
            "output": create_output + confirm_output.as_str() + promote_output.as_str(),
            "code": code,
        }))
    }
    fn promote_user(state: &Arc<AppState>, task: &Task) -> TaskResult {
        let params = TaskParams::new(task)?;
        let (output, code) = app_command(
            state,
            task,
            &[
                "admin",
                "account",
                "promote",
                "confirm",
                "--username",
                params.req_str("username")?,
            ],
        )?;
        Ok(serde_json::json!({
            "output": output,
            "code": code,
        }))
    }
    fn export(_state: &Arc<AppState>, _task: &Task) -> TaskResult {
        unimpl()
    }
    fn prune_caches(_state: &Arc<AppState>, _task: &Task) -> TaskResult {
        unimpl()
    }
}
