use anyhow::{anyhow, Result};
use serde_json::Value;
use std::collections::HashMap;

use crate::config::InstanceConfig;
use crate::land::pg::PgConfig;
use crate::land::S3Config;
use crate::state::AppState;

pub fn build_config(
    state: &AppState,
    inst: &InstanceConfig,
) -> Result<HashMap<String, serde_json::Value>> {
    let pg_config = PgConfig::from_inst(state, inst)?.with_bouncer(state);
    log::trace!("pg config: {pg_config:?}");

    let mut map: HashMap<String, serde_json::Value> = HashMap::new();

    let param_bool = |key: &str| -> Value {
        match inst.env.get(key) {
            Some(v) => Value::Bool(v == "true"),
            None => Value::Bool(false),
        }
    };

    // ~~~ s3 storage
    let s3_config = S3Config::from_inst(&state, &inst)?;
    log::trace!("s3 config: {s3_config:?}");

    map.insert("storage-backend".to_owned(), "s3".into());
    map.insert(
        "storage-s3-endpoint".into(),
        Value::String(s3_config.host_address.clone()),
    );
    map.insert(
        "storage-s3-access-key".into(),
        Value::String(s3_config.credentials.key_id.clone()),
    );
    map.insert(
        "storage-s3-secret-key".into(),
        Value::String(s3_config.credentials.secret.clone()),
    );
    map.insert(
        "storage-s3-bucket".into(),
        Value::String(s3_config.bucket.clone()),
    );

    // ~~~ accounts
    // https://docs.gotosocial.org/en/latest/configuration/accounts/
    map.insert(
        "accounts-registration-open".into(),
        param_bool("ACC_REGISTRATIONS_OPEN"),
    );
    map.insert(
        "accounts-approval-required".into(),
        param_bool("ACC_APPROVAL_REQUIRED"),
    );
    map.insert(
        "accounts-reason-required".into(),
        param_bool("ACC_REASON_REQUIRED"),
    );
    map.insert(
        "accounts-allow-custom-css".into(),
        param_bool("ALLOW_CUSTOM_CSS"),
    );
    map.insert("instance-expose-peers".into(), param_bool("EXPOSE_PEERS"));
    map.insert(
        "instance-expose-suspended".into(),
        param_bool("EXPOSE_SUSPENDED"),
    );

    // ~~~ database
    // https://docs.gotosocial.org/en/latest/configuration/database/
    map.insert("db-address".into(), Value::String(pg_config.host.clone()));
    map.insert("db-port".into(), Value::Number(pg_config.port.into()));
    map.insert("db-user".into(), Value::String(pg_config.username.clone()));
    map.insert(
        "db-password".into(),
        Value::String(pg_config.password.clone()),
    );
    map.insert(
        "db-database".into(),
        Value::String(pg_config.dbname.clone()),
    );

    // https://docs.gotosocial.org/en/latest/configuration/general/
    map.insert("log-level".into(), "info".into());
    map.insert("host".into(), inst.domain.clone().into());
    map.insert("account-domain".into(), inst.domain.clone().into());
    map.insert(
        "port".into(),
        Value::Number((20_000 + inst.local_id).into()),
    );
    map.insert("trusted-proxies".into(), "10.0.0.0/8".into());

    Ok(map)
}
