use crate::land::*;
use crate::state::AppState;
use crate::task::def::TaskResult;
use crate::task::{Task, TaskError};
use anyhow::{anyhow, Context};
use std::path::PathBuf;
use std::sync::Arc;

use super::config::build_config;
use crate::land::pg::PgConfig;

pub fn path_push(pb: &PathBuf, seg: &str) -> PathBuf {
    let mut pb = pb.clone();
    pb.push(seg);
    pb
}

pub fn instance_setup<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();

    let inst_name = &task.instance;
    let inst = state.get_instance(inst_name).unwrap();

    // bouncer-less pg connection for setup
    let pg_config = PgConfig::from_inst(state, &inst)?;

    // ~~~ instance user
    let username = crate::land::runner::get_username(inst_name, &inst);
    log::debug!("creating instance user ({username})");
    changes.push(
        crate::land::system::setup_user::<T>(state, task, &username)?,
        "user",
    );

    // ~~~ build environment file (empty)
    let env_path = PathBuf::from(format!("/etc/troupo/env/{inst_name}"));
    changes.push(
        T::set_text(&env_path, "").context("writing instance env file")?,
        &format!("/etc/troupo/env/{inst_name}"),
    );

    let home = PathBuf::from(format!("/home/{username}/"));
    let home_str = home.to_string_lossy().into_owned();
    let config_path = path_push(&home, "config.json");
    let config_path_str = config_path.to_string_lossy();
    let gts_bin_path = path_push(&home, "gotosocial");
    let gts_bin_path_str = gts_bin_path.to_string_lossy().into_owned();
    let gts_archive_path = path_push(&home, "gotosocial.tar.gz");
    let gts_archive_path_str = gts_archive_path.to_string_lossy().into_owned();

    // ~~~ fetch binary
    let version_conf = state
        .startup_config
        .versions
        .gotosocial
        .get(&inst.version)
        .ok_or(anyhow!("version not found"))?;
    let archive_resp =
        reqwest::blocking::get(&version_conf.dist_url).context("failed to fetch gts binary")?;
    let archive_data = archive_resp
        .bytes()
        .context("failed to decode gts binary")?;
    let archive_hash = ring::digest::digest(&ring::digest::SHA256, &archive_data);
    let expected_hash =
        hex::decode(&version_conf.dist_sha256).context("failed to parse expected hash")?;

    if archive_hash.as_ref() != expected_hash {
        return Err(TaskError::from(anyhow!(
            "gts binary downloaded does not match expected hash!"
        )));
    }

    T::set_file(&gts_archive_path, &archive_data)?;
    T::run_as_root(
        state,
        task,
        &["tar", "xzvf", &gts_archive_path_str, "-C", &home_str],
    )?;

    // ~~~ config file
    let config = serde_json::to_string_pretty(&build_config(&state, &inst)?)
        .context("serializing gts config")?;
    changes.push(
        T::set_text(&config_path, &config).context("writing instance env file")?,
        &format!("GoToSocial config to {config_path:?}"),
    );
    T::run_as_root(
        state,
        task,
        &[
            "chown",
            &format!("{username}:{username}"),
            &config_path.to_string_lossy().into_owned(),
        ],
    )?;
    T::run_as_root(
        state,
        task,
        &["chmod", "600", &config_path.to_string_lossy().into_owned()],
    )?;

    // ~~~ database
    log::debug!("creating database user");
    crate::land::pg::setup::<T>(&pg_config, &mut changes)?;
    pgbouncer::setup::<T>(state, task, &mut changes, "session")?;

    // ~~~ install services
    let svc_key = format!("gts-{inst_name}.service");
    let svc_path = PathBuf::from(format!("/etc/systemd/system/gts-{inst_name}.service"));

    let svc_desc = ServiceBuilder::simple(&svc_key)
        .with_user(&username)
        .with_dir(&home.to_string_lossy())
        .with_exec_start(&format!(
            "{gts_bin_path_str} --config-path {config_path_str} server start"
        ))
        .with_exec_reload("/bin/kill -SIGUSR1 $MAINPID")
        .with_max_mem(1500)
        .with_high_mem(800)
        .build();

    changes.push(
        T::set_text(&svc_path, &svc_desc)
            .with_context(|| format!("writing svc {svc_key} for inst {inst_name}"))?,
        &svc_key,
    );

    // start & enable services
    T::run_as_root(state, task, &["systemctl".into(), "daemon-reload".into()])?;
    if T::svc_is_enabled(state, task, &svc_key)? {
        T::run_as_root(state, task, &["systemctl", "restart", &svc_key])?;
    } else {
        T::run_as_root(state, task, &["systemctl", "enable", "--now", &svc_key])?;
    }

    Ok(changes)
}

pub fn instance_remove<T: Actor>(state: &Arc<AppState>, task: &Task) -> TaskResult<ChangeSet> {
    let mut changes = ChangeSet::new();

    let inst_name = &task.instance;
    let svc_key = format!("gts-{inst_name}.service");

    // clean up services
    let svc_path = PathBuf::from(format!("/etc/systemd/system/{svc_key}.service"));

    T::run_as_root(state, task, &["systemctl", "disable", "--now", &svc_key])?;
    changes.push(T::delete(&svc_path)?, &format!("delete service {svc_key}"));

    // update pgbouncer
    pgbouncer::setup::<T>(state, task, &mut changes, "session")?;

    Ok(changes)
}
