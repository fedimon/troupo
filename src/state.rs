use chrono::Utc;
use std::sync::atomic::AtomicU16;
use std::sync::{Arc, Mutex};
use sysinfo::SystemExt;
use tokio::sync::mpsc;
use uuid::Uuid;

use crate::config::{InstanceConfig, MutableConfigWrapper, StartupConfig};
use crate::task::{Task, TaskState};

pub type TaskSender = mpsc::Sender<Uuid>;
pub type TaskReceiver = mpsc::Receiver<Uuid>;


/// our great state, shared between every thread, transparent and fairly
/// accessible and mutable to all who need it thru easy methods
pub struct AppState {
    pub host_name: String,
    pub startup_config: StartupConfig,
    pub runtime_config: MutableConfigWrapper,
    /// Execution queue for pending tasks
    pub task_queue: mpsc::Sender<Uuid>,
    pub instance_serial_id: AtomicU16,
    pub host_system: Mutex<sysinfo::System>,
}
impl AppState {
    pub fn open(startup_config: StartupConfig, task_sender: TaskSender) -> Self {
        Self {
            host_name: startup_config.system.host_name.clone(),
            startup_config,
            runtime_config: MutableConfigWrapper::load(),
            task_queue: task_sender,
            instance_serial_id: AtomicU16::new(1),
            host_system: Mutex::new(sysinfo::System::new()),
        }
    }

    pub fn get_instances(&self) -> Vec<String> {
        self.runtime_config
            .get_with(|c| c.instances.keys().cloned().collect())
    }

    pub fn get_instance(&self, id: &str) -> Option<InstanceConfig> {
        self.runtime_config
            .get_with(|c| c.instances.get(&id.to_owned()).cloned())
    }

    pub fn mut_instance<F: FnOnce(&mut InstanceConfig)>(
        &self,
        id: &str,
        f: F,
    ) -> Option<InstanceConfig> {
        self.runtime_config.mut_with(|c| {
            c.instances
                .get_mut(id)
                .map(|i| {
                    f(i);
                    i
                })
                .cloned()
        })
    }
    pub fn del_instance(&self, id: &str) -> bool {
        self.runtime_config.mut_with(|c| {
            let removed = c.instances.remove(id).is_some();

            // delete linked tasks
            let tasks: Vec<Uuid> = c
                .tasks
                .iter()
                .filter(|(_, t)| t.instance == id)
                .map(|(i, _)| i.clone())
                .collect();
            for task_id in tasks {
                c.tasks.remove(&task_id);
            }
            removed
        })
    }

    pub fn get_task(&self, id: Uuid) -> Option<Task> {
        self.runtime_config.get_with(|c| c.tasks.get(&id).cloned())
    }

    pub fn mut_task<F: FnOnce(&mut Task)>(&self, id: &Uuid, f: F) -> Option<Task> {
        self.runtime_config.mut_with(|c| {
            c.tasks
                .get_mut(&id)
                .map(|i| {
                    f(i);
                    i.update_date = Some(Utc::now());
                    i
                })
                .cloned()
        })
    }

    pub fn add_task(&self, task: &Task) {
        log::debug!(
            "adding task {} ({:?}, {:?})",
            task.id,
            task.task_type,
            task.state
        );

        self.runtime_config.mut_with(|c| {
            c.tasks.insert(task.id, task.clone());
        });
    }

    pub fn get_instance_tasks(&self, instance_name: &str) -> anyhow::Result<Vec<Task>> {
        let list = self.runtime_config.get_with(|c| {
            c.tasks
                .iter()
                .filter_map(|(_id, task)| {
                    if task.instance == instance_name {
                        Some(task.clone())
                    } else {
                        None
                    }
                })
                .collect()
        });
        Ok(list)
    }

    pub fn get_tasks(&self) -> anyhow::Result<Vec<Task>> {
        let list = self
            .runtime_config
            .get_with(|c| c.tasks.values().map(ToOwned::to_owned).collect());
        Ok(list)
    }

    pub fn list_instances_with_scheduler(&self) -> Vec<String> {
        self.runtime_config
            .config
            .read()
            .instances
            .iter()
            .filter_map(|(key, inst)| {
                if !inst.run_scheduler {
                    return None;
                }
                Some(key.clone())
            })
            .collect()
    }
}

/// code to be run after calling add_task()
/// it is split off because it's async and needs an Arc<AppState>
/// (insteead of a ref)
pub async fn on_task_add(state: Arc<AppState>, task: &Task) {
    if task.state == TaskState::Pending {
        // add to exec queue
        state.task_queue.send(task.id).await.unwrap();
    }
}
