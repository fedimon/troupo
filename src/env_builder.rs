//! manages environment variables
//! (safely building env/* files)
use crate::config::{InstanceConfig, StartupConfig};
use crate::state::AppState;
use anyhow::{anyhow, bail, Context, Result};
use rand::distributions::{Distribution, Uniform};
use std::collections::HashMap;
use std::sync::Arc;

pub struct DbEnvKeySet {
    pub user: &'static str,
    pub pass: &'static str,
    pub name: &'static str,
}

/// a structure shared between env builders and many tasks
/// also contains an accumulator of built env variables
/// and some helpers to build envs
/// while species/+/env.rs adapts it to each actual env keys
pub struct EnvBuilder {
    pub state: Arc<AppState>,
    pub instance: String,
    pub instance_config: InstanceConfig,
    pub local_config: StartupConfig,
    pub env: HashMap<String, String>,
}
impl EnvBuilder {
    pub fn new(state: &Arc<AppState>, inst_name: &str) -> Result<Self> {
        Ok(Self {
            state: state.clone(),
            instance: inst_name.to_owned(),
            instance_config: state
                .get_instance(inst_name)
                .ok_or_else(|| anyhow!("instance {} not found", inst_name))?,
            local_config: state.startup_config.clone(),
            env: HashMap::new(),
        })
    }

    pub fn build(
        state: &Arc<AppState>,
        inst_name: &str,
        builder: fn(&mut EnvBuilder) -> Result<()>,
    ) -> Result<Self> {
        let mut ctx = crate::env_builder::EnvBuilder::new(state, inst_name)
            .context("initializing env builder")?;
        builder(&mut ctx)?;
        log::trace!("built env: {:?}", ctx.env);
        Ok(ctx)
    }

    pub fn add<F: Fn(&Self) -> Result<Vec<(String, String)>>>(&mut self, f: F) -> Result<()> {
        let name = std::any::type_name::<F>();
        self.env
            .extend(f(self).context(format!("failed to build {name} env"))?);
        Ok(())
    }

    pub fn into(self) -> HashMap<String, String> {
        self.env
    }

    pub fn get_sat_config<'a, T>(
        &self,
        key_param: &str,
        map: &'a HashMap<String, T>,
    ) -> Result<&'a T> {
        let db_config = self.instance_config.satellite_conf(key_param, map)?;
        Ok(db_config)
    }

    pub fn get_pg_keys(&self) -> DbEnvKeySet {
        match self.instance_config.species {
            crate::config::Species::Mastodon => DbEnvKeySet {
                user: "DB_USER",
                pass: "DB_PASS",
                name: "DB_NAME",
            },
            crate::config::Species::Pixelfed => DbEnvKeySet {
                user: "DB_USERNAME",
                pass: "DB_PASSWORD",
                name: "DB_DATABASE",
            },
            crate::config::Species::GoToSocial => DbEnvKeySet {
                user: "DB_USER",
                pass: "DB_PASS",
                name: "DB_NAME",
            },
        }
    }
    // TODO use the same DbEnvKeySet struct, add host/port
    pub fn get_pg_values(&self) -> Result<(String, String, String)> {
        let keys = self.get_pg_keys();

        let user = self.env.get(keys.user).ok_or(anyhow!("missing DB_USER"))?;
        let pass = self.env.get(keys.pass).ok_or(anyhow!("missing DB_PASS"))?;
        let name = self.env.get(keys.name).ok_or(anyhow!("missing DB_NAME"))?;
        Ok((user.clone(), pass.clone(), name.clone()))
    }

    pub fn get_value(&self, key: &str) -> Option<String> {
        if let Some(v) = self.instance_config.env.get(key) {
            if !v.is_empty() {
                log::trace!("using {} from inst cfg: {}", key, v);
                return Some(v.clone());
            }
        }
        if let Some(v) = self.local_config.common_env.get(key) {
            log::trace!("using {} from local cfg: {}", key, v);
            return Some(v.clone());
        }
        None
    }

    pub fn get_value_or(&self, key: &str, default: &str) -> String {
        self.get_value(key).unwrap_or_else(|| default.to_owned())
    }

    pub fn get_secret(&self, key: &str) -> Result<Option<String>> {
        // try from plain variables first
        if let Some(v) = self.get_value(key) {
            log::trace!("using {key} from inst params");
            return Ok(Some(v));
        }

        // try from vault
        let config = match &self.state.startup_config.vault {
            Some(config) => config,
            None => {
                // vault is not enabled
                return Ok(None);
            }
        };
        return crate::secrets::query(config, &self.instance, key)
    }

    pub fn get_secret_or_else<F: Fn() -> String>(&self, key: &str, default_f: F) -> Result<String> {
        if let Some(v) = self.get_secret(key)? {
            return Ok(v);
        }

        log::trace!("generating {key}");
        let new_value = default_f();
        if let Err(e) = crate::secrets::save(&self.state, &self.instance, key, &new_value) {
            log::warn!("failed to save secret in vault: {e}");
            bail!("failed to save secret in vault: {e}");
        }

        Ok(new_value)
    }
    pub fn pass_value(&self, key: &str) -> Result<(String, String)> {
        Ok((
            key.into(),
            self.get_value(key)
                .with_context(|| format!("failed to get {key}"))?,
        ))
    }
    pub fn pass_value_or(&self, key: &str, default: &str) -> (String, String) {
        (key.into(), self.get_value_or(key, default))
    }

    /// pass a secret - get it from overrides, or vault
    /// generates it and stores it in vault if not found
    pub fn pass_secret_or_gen<F: Fn() -> String>(
        &self,
        key: &str,
        default_f: F,
    ) -> Result<(String, String)> {
        Ok((key.into(), self.get_secret_or_else(key, default_f)?))
    }

    /// pass a secret - get it from overrides, or vault
    /// use a default if not found
    pub fn pass_secret_or(&self, key: &str, default: &str) -> Result<(String, String)> {
        Ok((
            key.into(),
            self.get_secret(key)?.unwrap_or_else(|| default.into()),
        ))
    }

    pub fn pass_secret_or_use(
        &self,
        key: &str,
        default: Option<&String>,
    ) -> Result<(String, String)> {
        self.get_secret(key)?
            .or(default.map(ToOwned::to_owned))
            .map(|v| (key.into(), v))
            .ok_or(anyhow!("missing {key}"))
    }
}

pub fn add_prefix(value: &str, prefix: &Option<String>) -> String {
    let mut s = String::new();
    if let Some(prefix) = prefix {
        s.insert_str(0, &prefix);
    }
    s.push_str(value);
    s
}

use base64::Engine;

pub fn keygen_vapid() -> String {
    let rng = &mut rand::rngs::OsRng;
    let private_key = ecdsa::SigningKey::<p256::NistP256>::random(rng);

    base64::engine::general_purpose::URL_SAFE.encode(private_key.to_bytes())
}

pub fn vapid_public(private_key: &str) -> Result<String> {
    let private_bytes = base64::engine::general_purpose::URL_SAFE
        .decode(private_key)
        .map_err(|e| anyhow!("failed to decode private vapid key: {e}"))?;
    let private_key =
        ecdsa::SigningKey::<p256::NistP256>::from_bytes(private_bytes.as_slice().into())?;
    let public_key = private_key.verifying_key();
    Ok(base64::engine::general_purpose::URL_SAFE.encode(public_key.to_encoded_point(false)))
}

pub fn keygen(length: usize) -> String {
    let rng = &mut rand::rngs::OsRng;
    let charset: Vec<char> = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        .chars()
        .collect();
    let uniform = Uniform::from(0..charset.len());
    (0..length).map(|_| charset[uniform.sample(rng)]).collect()
}

pub fn keygen_generic() -> String {
    keygen(64)
}

pub fn keygen_password() -> String {
    keygen(24)
}
