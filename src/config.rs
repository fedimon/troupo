use anyhow::{anyhow, bail, Context, Result};
use chrono::{Duration, Utc};
use parking_lot::RwLock;
use regex::Regex;
use serde::{Deserialize, Deserializer, Serialize};
use serde_enum_str::{Deserialize_enum_str, Serialize_enum_str};
use std::collections::HashMap;
use std::fmt::Debug;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use std::time::Duration as SDuration;

fn deserialize_duration<'de, D>(deserializer: D) -> Result<std::time::Duration, D::Error>
where
    D: Deserializer<'de>,
{
    let buf = String::deserialize(deserializer)?;

    let seconds_re = Regex::new("([9-0]+) sec(ond)?s").unwrap();

    let d = if let Some(c) = seconds_re.captures(&buf) {
        let n = c.get(1).unwrap();
        std::time::Duration::from_secs(n.as_str().parse().unwrap())
    } else {
        return Err(serde::de::Error::custom(format!(
            "failed to parse duration"
        )));
    };

    Ok(d)
}

macro_rules! default_val {
    ($key:ident, $typ:path, $value:expr) => {
        fn $key() -> $typ {
            $value.into()
        }
    };
}


default_val!(val_localhost, String, "localhost");
default_val!(val_pgb_port, u16, 6432u16);
default_val!(
    val_registry_timeout,
    std::time::Duration,
    std::time::Duration::from_secs(5)
);
default_val!(
    val_task_expiration_time,
    std::time::Duration,
    std::time::Duration::from_secs(60 * 60 * 24)
);
default_val!(val_media_cache_days, u16, 90u16);
default_val!(val_status_cache_days, u16, 180u16);
//default_val!(, , );

default_val!(val_registry_pull_interval, SDuration, SDuration::from_secs(30));
default_val!(val_registry_push_interval, SDuration, SDuration::from_secs(5));

fn local_hostname() -> String {
    gethostname::gethostname().to_string_lossy().into_owned()
}

pub mod sats {
    use super::{Deserialize, Serialize};

    default_val!(pg_admin_user, String, "postgres");
    default_val!(s3_default_protocol, String, "https");
    default_val!(s3_export_expiry, u32, 604800u32);
    default_val!(s3_default_proxy_host, String, "{inst}.files.fedi.monster");
    default_val!(export_tmp_dir, String, "/var/cache/troupo/");
    default_val!(export_path_prefix, String, "/fedimonster/export/");
    //default_val!(,,);

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct PostgresConfig {
        pub host: String,
        pub port: Option<u16>,
        #[serde(default = "pg_admin_user")]
        pub admin_username: String,
        pub admin_password: Option<String>,
        pub db_prefix: Option<String>,
        pub user_prefix: Option<String>,
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct StorageConfig {
        /// protocol used by troupo and apps to communicate with s3 directly -
        /// the proxy will have its own settings
        #[serde(default = "s3_default_protocol")]
        pub protocol: String,
        pub host: String,
        pub region: String,
        pub proxy_address: Option<String>,
        #[serde(default = "s3_default_proxy_host")]
        pub proxy_host: String,
        // DEPRECATED
        pub shared_key_id: Option<String>,
        pub shared_secret: Option<String>,
    }

    // a s3 profile with a key allowed to push to a specific bucket
    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct ExportConfig {
        #[serde(default = "s3_default_protocol")]
        pub protocol: String,
        pub host: String,
        pub region: String,
        pub key_id: String,
        pub secret: String,
        pub bucket_name: String,
        #[serde(default = "export_path_prefix")]
        pub path_prefix: String,
        #[serde(default = "s3_export_expiry")]
        pub default_expiry: u32,
        #[serde(default = "export_tmp_dir")]
        pub tmp_dir_path: String,
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct RedisConfig {
        pub host: String,
        pub port: Option<u16>,
        pub password: Option<String>,
        pub namespace: Option<String>,
        pub invididualized: Option<bool>,
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct MailConfig {
        pub host: String,
        pub port: u16,
        pub login: String,
        pub password: String,
        /// defaults to: notices-{inst}@instmail.maas.network
        pub from_address: Option<String>,
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct ElaSearchConfig {
        pub host: String,
        pub port: Option<u16>,
    }
}

pub mod species {
    use super::{Deserialize, Serialize};
    use std::collections::HashMap;

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct GoToSocialConfig {
        pub dist_url: String,
        pub dist_sha256: String,
    }

    #[derive(Debug, Clone, Serialize, Deserialize, Default)]
    pub struct SpeciesConfig {
        pub mastodon: HashMap<String, String>,
        /// values are branch names on git
        pub pixelfed: HashMap<String, String>,
        pub gotosocial: HashMap<String, GoToSocialConfig>,
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TaskRegistry {
    pub url: String,
    pub auth: String,
    #[serde(default = "val_registry_timeout")]
    #[serde(deserialize_with = "deserialize_duration")]
    pub timeout: std::time::Duration,

    /// fresh state will be periodically pulled from the registry
    #[serde(default = "val_registry_pull_interval")]
    #[serde(deserialize_with = "deserialize_duration")]
    pub pull_interval: std::time::Duration,

    /// only used when changes are queued, this is the highest freq at which
    /// we will push instance and task updates.
    #[serde(default = "val_registry_push_interval")]
    #[serde(deserialize_with = "deserialize_duration")]
    pub push_interval: std::time::Duration,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PgBouncer {
    #[serde(default = "val_localhost")]
    pub host: String,
    #[serde(default = "val_pgb_port")]
    pub port: u16,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VaultConfig {
    pub url: String,
    pub token: String,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Satellites {
    pub postgres: HashMap<String, sats::PostgresConfig>,
    pub storage: HashMap<String, sats::StorageConfig>,
    pub redis: HashMap<String, sats::RedisConfig>,
    pub mail: HashMap<String, sats::MailConfig>,
    pub elasearch: HashMap<String, sats::ElaSearchConfig>,
}
impl Satellites {
    pub fn keys(&self) -> HashMap<String, Vec<String>> {
        fn keys<T>(map: &HashMap<String, T>) -> Vec<String> {
            map.keys().map(|x| x.clone()).collect()
        }
        let mut m = HashMap::new();
        m.insert("postgres".into(), keys(&self.postgres));
        m.insert("storage".into(), keys(&self.storage));
        m.insert("redis".into(), keys(&self.redis));
        m.insert("mail".into(), keys(&self.mail));
        m.insert("elasearch".into(), keys(&self.elasearch));
        m
    }
}

/// everything else that isnt related to an external tool
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SystemSettings {
    #[serde(default = "local_hostname")]
    pub host_name: String,
    #[serde(default = "val_media_cache_days")]
    pub media_cache_days: u16,
    #[serde(default = "val_status_cache_days")]
    pub status_cache_days: u16,
    #[serde(default = "val_task_expiration_time")]
    #[serde(with = "humantime_serde")]
    pub task_expiration_time: std::time::Duration,
}
impl Default for SystemSettings {
    fn default() -> Self {
        Self {
            host_name: local_hostname(),
            media_cache_days: val_media_cache_days(),
            status_cache_days: val_status_cache_days(),
            task_expiration_time: val_task_expiration_time(),
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct StartupConfig {
    #[serde(default)]
    pub system: SystemSettings,
    pub versions: species::SpeciesConfig,
    pub task_registry: Option<TaskRegistry>,
    #[serde(default)]
    pub api_keys: HashMap<String, String>,
    #[serde(default)]
    pub common_env: HashMap<String, String>,
    pub vault: Option<VaultConfig>,
    #[serde(default)]
    pub bin_paths: HashMap<String, String>,
    pub pgbouncer: Option<PgBouncer>,
    pub export: Option<sats::ExportConfig>,
    pub satellites: Satellites,
}
impl StartupConfig {
    pub fn get() -> Result<Self> {
        let path = std::env::var("TROUPO_CONFIG").unwrap_or("/etc/troupo/troupo.toml".to_owned());
        let mut troupo_f = std::fs::File::open(path)?;

        let mut contents = String::new();
        troupo_f
            .read_to_string(&mut contents)
            .map_err(|e| anyhow!("Could not read config file: {:?}", e))?;

        let obj: StartupConfig = toml::from_str(&contents)
            .map_err(|e| anyhow!("Could not parse startup file: {:?}", e))?;

        Ok(obj)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, FromForm, Default)]
pub struct InstanceStats {
    pub media_size_mb: usize,
    pub db_size_mb: usize,
    pub pending_tasks: usize,
    pub active_users: usize,
    pub total_users: usize,
    pub media_cache_ratio: f32,
}
impl InstanceStats {
    pub fn sum(items: &[Self]) -> Self {
        Self {
            media_size_mb: items.iter().map(|i| i.media_size_mb).sum(),
            db_size_mb: items.iter().map(|i| i.db_size_mb).sum(),
            pending_tasks: items.iter().map(|i| i.pending_tasks).sum(),
            active_users: items.iter().map(|i| i.active_users).sum(),
            total_users: items.iter().map(|i| i.total_users).sum(),
            media_cache_ratio: items
                .iter()
                .map(|i| i.media_cache_ratio as f32)
                .sum::<f32>()
                / items.len() as f32,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WebScale {
    pub mem_mb: usize,
    pub processes: usize,
    pub min_threads: usize,
    pub max_threads: usize,
}
impl Default for WebScale {
    fn default() -> Self {
        Self {
            mem_mb: 800,
            processes: 0,
            min_threads: 2,
            max_threads: 8,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, FromFormField, PartialEq)]
pub enum WorkerModel {
    Single,
    Heavy,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorkerScale {
    pub model: WorkerModel,
    pub mem_mb: usize,
    pub threads: usize,
}
impl Default for WorkerScale {
    fn default() -> Self {
        Self {
            model: WorkerModel::Single,
            mem_mb: 600,
            threads: 10,
        }
    }
}

#[derive(
    Debug, Copy, Clone, Serialize_enum_str, Deserialize_enum_str, FromFormField, PartialEq, Eq,
)]
#[serde(rename_all = "snake_case")]
pub enum Species {
    Mastodon,
    Pixelfed,
    GoToSocial,
}
impl Default for Species {
    fn default() -> Self {
        Self::Mastodon
    }
}

/// opreational status of the instance. sorry for the confusing name
/// here goes exported runtime variables, ping result, whatever else?
/// they will be set by crate::land and crate::species
#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct InstanceStatus {
    /// effective data storage host set by the storage satellite
    /// the s3 region endpoint
    pub storage_host: Option<String>,
    /// front facing proxy hostname
    pub storage_proxy: Option<String>,
}
impl InstanceStatus {
    pub fn from_json_map(map: &HashMap<String, serde_json::Value>) -> Self {
        Self {
            storage_host: map.get("storage_host").and_then(|f| f.as_str()).map(|v| v.to_owned()),
            storage_proxy: map.get("storage_proxy").and_then(|f| f.as_str()).map(|v| v.to_owned()),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InstanceConfig {
    pub name: String,
    /// a small integer, used to generate local port numbers (<1000)
    pub local_id: u16,
    pub species: Species,
    /// version name, to be found in startup_config.versions
    /// (it is Not validated before use; an unknown version is to be ignored)
    pub version: String,
    pub domain: String,
    pub web_scale: WebScale,
    pub worker_scale: WorkerScale,
    /// only a single troupo in a cluster should have this
    pub run_scheduler: bool,
    pub env: HashMap<String, String>,
    pub satellites: HashMap<String, String>,
    /// numeric values from the instance
    pub stats: Option<InstanceStats>,
    /// reported values, generated on deployment or specific events
    /// really this is just a second stats field but More of it
    pub status: InstanceStatus,

    pub periodic_tasks: TaskScheduler,

    /// a special internal value (never exported) to track changes on this
    /// structure. true means it should be pushed to registry at the next beat.
    /// it should be set when .stats or .status are updated
    pub locally_updated: bool,
}
impl InstanceConfig {
    pub fn new(
        name: String,
        local_id: u16,
        species: Species,
        version: String,
        domain: String,
    ) -> Self {
        Self {
            name: name,
            local_id,
            web_scale: WebScale::default(),
            worker_scale: WorkerScale::default(),
            stats: None,
            status: Default::default(),
            env: HashMap::new(),
            satellites: HashMap::new(),
            periodic_tasks: TaskScheduler::new(),
            run_scheduler: true,
            locally_updated: false,
            species,
            version,
            domain,
        }
    }
    pub fn satellite<'a>(&'a self, key: &str) -> &'a str {
        self.satellites
            .get(key)
            .or_else(|| {
                log::trace!("sat {key} not found; trying as env.{key}_sat");
                self.env.get(&format!("{key}_sat"))
            })
            .map(String::as_str)
            .unwrap_or(&"default")
    }
    pub fn satellite_conf<'a, T>(&self, key: &str, map: &'a HashMap<String, T>) -> Result<&'a T> {
        let sat_key = self.satellite(key);
        let db_config = map.get(&sat_key.to_owned());

        let db_config = if let Some(c) = db_config {
            c
        } else {
            log::warn!(
                "inst {}: sat {:?} not found ({})",
                self.local_id,
                sat_key,
                std::any::type_name::<T>()
            );
            return Err(anyhow!(
                "inst {}: sat {:?} not found",
                self.local_id,
                sat_key
            ));
        };
        Ok(db_config)
    }
    pub fn greate_secret<F: FnOnce() -> String>(
        &self,
        state: &AppState,
        key: &str,
        gen: F,
    ) -> Result<String> {
        if let Some(v) = self.env.get(key) {
            log::trace!("using {key} from inst params");
            return Ok(v.clone());
        }
        let config = match &state.startup_config.vault {
            Some(config) => config,
            None => {
                // vault is disabled and we do not have a password as env:
                // we generate a disposable password
                log::warn!("no vault - generating a temporary value for {key}");
                return Ok(gen());
            }
        };
        match crate::secrets::query(config, &self.name, key) {
            // connection to vault failed
            Err(e) => {
                bail!("failed to query vault: {e}");
            },
            // valid password in vault
            Ok(Some(v)) => {
                log::trace!("using {key} from vault");
                return Ok(v);
            },
            // good but empty answer: generate and save
            Ok(None) => {},
        }
        log::trace!("generating a new {key}");
        let value = gen();

        crate::secrets::save(state, &self.name, key, &value)
            .context("failed to save secret in vault")?;

        return Ok(value);
    }

    pub fn mem_allocation_mb(&self) -> usize {
        match self.species {
            Species::Mastodon => {
                self.web_scale.mem_mb + self.worker_scale.mem_mb + 300 // streaming service
            }
            Species::Pixelfed => {
                768 * 2 // web server max for half the total threads - max * max would be a burst
                + 1000 // workers idk
            }
            Species::GoToSocial => 800,
        }
    }
}
use rand::distributions::{Bernoulli, Distribution};

use crate::state::AppState;
#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct TaskScheduler {
    last_run: HashMap<String, chrono::DateTime<Utc>>,
}
impl TaskScheduler {
    pub fn new() -> Self {
        Self {
            last_run: HashMap::new(),
        }
    }

    /// if it should be executed, it returns true and updates the
    /// last_run timestamp. otherwise, returns false
    pub fn run(&mut self, task_name: &str, delay: Duration) -> bool {
        let last = self.last_run.get(task_name);

        if let Some(v) = last {
            if Utc::now().signed_duration_since(*v) >= delay {
                self.bump(task_name);
                return true;
            }
            return false;
        }

        // when no value is known, we wait a random time between 0 and typical
        // delay, as to not overload anything on first run.
        // assuming this is called every hour, and aiming for 75% at the half
        // of `delay`
        let p = (0.75f64).powf(delay.num_hours() as f64 / 2.0);
        let d = Bernoulli::new(p).unwrap();
        let v = d.sample(&mut rand::thread_rng());
        log::trace!("scheduler for {task_name}: run={v:?} p={p:.3}");
        if !v {
            return false;
        }

        self.bump(task_name);
        return true;
    }

    pub fn bump(&mut self, task_name: &str) {
        self.last_run.insert(task_name.to_owned(), Utc::now());
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct RuntimeConfig {
    pub instances: HashMap<String, InstanceConfig>,
    pub tasks: HashMap<uuid::Uuid, crate::task::Task>,
    pub periodic_tasks: TaskScheduler,
}

/// a wrapper to easily read/write the mutable runtime config
pub struct MutableConfigWrapper {
    pub path: PathBuf,
    pub config: RwLock<RuntimeConfig>,
}
impl MutableConfigWrapper {
    /// load from file or create an empty instance
    pub fn load() -> Self {
        let path =
            std::env::var("TROUPO_INSTANCES").unwrap_or("/etc/troupo/instances.ron".to_owned());
        let path = Path::new(&path);
        match Self::load_file(path) {
            Ok(v) => v,
            Err(e) => {
                log::warn!(
                    "could not load instances from {:?}: {}; using an empty state",
                    path,
                    e
                );
                Self::new(path)
            }
        }
    }

    /// create an empty config (to ben saved as `path`)
    pub fn new(path: &Path) -> Self {
        Self {
            path: path.to_owned(),
            config: RwLock::new(RuntimeConfig::default()),
        }
    }

    /// load an existing file
    pub fn load_file(path: &Path) -> Result<Self> {
        let mut f = File::open(path)?;

        let mut contents = String::new();
        f.read_to_string(&mut contents)
            .map_err(|e| anyhow!("Could not read config file: {:?}", e))?;

        let obj: RuntimeConfig = ron::from_str(&contents)
            .map_err(|e| anyhow!("Could not parse instances file: {:?}", e))?;

        log::info!("loaded {} instances from {:?}", obj.instances.len(), path);

        Ok(Self {
            path: path.to_owned(),
            config: RwLock::new(obj),
        })
    }

    pub fn save(&self) -> Result<()> {
        self._save(&*self.config.read())
    }

    pub fn _save(&self, c: &RuntimeConfig) -> Result<()> {
        let contents = ron::ser::to_string_pretty(
            &c,
            ron::ser::PrettyConfig::new()
                .compact_arrays(true)
                .struct_names(true),
        )?;
        let mut f = File::create(&self.path)?;
        f.write_all(contents.as_bytes())?;
        Ok(())
    }

    // --- a few wrappers to handle locking

    pub fn get_with<F: FnOnce(&RuntimeConfig) -> T, T>(&self, f: F) -> T {
        let guard = self.config.write();
        f(&*guard)
    }

    pub fn mut_with<F: FnOnce(&mut RuntimeConfig) -> T, T>(&self, f: F) -> T {
        let mut guard = self.config.write();
        let r = f(&mut *guard);
        if let Err(e) = self._save(&guard) {
            log::warn!("failed to save runtime config: {}", e);
        }
        r
    }

    pub fn try_mut_with<F: FnOnce(&mut RuntimeConfig) -> Result<T, anyhow::Error>, T>(
        &self,
        f: F,
    ) -> Result<T, anyhow::Error> {
        let mut guard = self.config.write();
        match f(&mut *guard) {
            Ok(r) => {
                self._save(&guard)?;
                Ok(r)
            }
            Err(e) => Err(e),
        }
    }
}
