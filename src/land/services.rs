pub struct ServiceBuilder {
    unit: String,
    service: String,
    install: String,
}
impl ServiceBuilder {
    pub fn simple(key: &str) -> Self {
        Self {
            unit: format!(
                "[Unit]
Description=mastodon-{key}
After=network.target"
            ),
            service: "[Service]
Type=simple
Restart=always
TimeoutSec=30
SyslogIdentifier=mastodon
CPUAccounting=yes
"
            .to_owned(),
            install: "[Install]
WantedBy=multi-user.target"
                .to_owned(),
        }
    }

    pub fn with_user(mut self, user: &str) -> Self {
        self.service += &format!("User={user}\n");
        self
    }
    pub fn with_dir(mut self, dir: &str) -> Self {
        self.service += &format!("WorkingDirectory={dir}\n");
        self
    }
    pub fn with_env(mut self, env: &str) -> Self {
        self.service += &format!("Environment=\"{env}\"\n");
        self
    }
    pub fn with_env_file(mut self, path: &str) -> Self {
        self.service += &format!("EnvironmentFile={path}\n");
        self
    }
    pub fn with_exec_start(mut self, cmd: &str) -> Self {
        self.service += &format!("ExecStart={cmd}\n");
        self
    }
    pub fn with_exec_reload(mut self, cmd: &str) -> Self {
        self.service += &format!("ExecReload={cmd}\n");
        self
    }
    pub fn with_max_mem(mut self, n: usize) -> Self {
        self.service += &format!("MemoryMax={n}M\n");
        self
    }
    pub fn with_high_mem(mut self, n: usize) -> Self {
        self.service += &format!("MemoryHigh={n}M\n");
        self
    }
    /// append a whole line to the service block and a |n
    pub fn with_service_attr(mut self, line: &str) -> Self {
        self.service += line;
        self.service += "\n";
        self
    }
    pub fn build(self) -> String {
        format!("{}\n{}\n{}", self.unit, self.service, self.install)
    }
}
