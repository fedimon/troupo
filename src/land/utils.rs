use crate::state::AppState;
use redis::Commands;
use std::collections::HashMap;
use which::which;

pub fn get_bin_path(state: &AppState, inst_name: &str, name: &str) -> String {
    let instance = match state.get_instance(&inst_name) {
        Some(i) => i,
        None => {
            log::warn!("no instance {:?} found", inst_name);
            return name.to_string();
        }
    };
    let user = &instance.version;

    let name = state
        .startup_config
        .bin_paths
        .get(name)
        .cloned()
        .unwrap_or_else(|| name.to_owned())
        .replace("~", &format!("/home/{user}"));

    if std::fs::metadata(&name).is_ok() {
        return name;
    }

    if let Ok(path) = which(&name) {
        return path.to_string_lossy().into_owned();
    }

    return name;
}

pub fn serialize_env(map: &HashMap<String, String>) -> String {
    let mut items: Vec<(&String, &String)> = map.iter().collect();
    items.sort();
    items
        .into_iter()
        .map(|(key, value)| format!("{key}={value}"))
        .collect::<Vec<String>>()
        .join("\n")
}
