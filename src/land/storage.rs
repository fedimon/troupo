use crate::config::InstanceConfig;
use crate::state::AppState;
use anyhow::{Context, Result};

#[derive(Clone)]
pub struct S3Credentials {
    pub key_id: String,
    pub secret: String,
}
impl S3Credentials {
    pub fn new(key_id: String, secret: String) -> Self {
        Self { key_id, secret }
    }
}

/// S3 configuration context for an instance and inheriting local properties
pub struct S3Config {
    pub bucket: String,
    pub protocol: String,
    pub region: String,
    pub host_address: String,
    pub proxy_address: String,
    /// key in used by the application.
    /// its not assumed to be dedicated or exportable or manage the bucket itself
    pub credentials: S3Credentials,
    /// dedicated key that can be exported in backups
    pub instance_credentials: Option<S3Credentials>,
    /// a shared key that can be used but not exported
    pub shared_credentials: Option<S3Credentials>,
}

impl std::fmt::Debug for S3Config {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "S3 {{ {}://{}@{}/{} using {} }}",
            self.protocol,
            self.credentials.key_id,
            self.host_address,
            self.bucket,
            self.proxy_address
        )
    }
}

impl S3Config {
    pub fn from_inst(state: &AppState, inst: &InstanceConfig) -> Result<Self> {
        let sat = inst.satellite_conf("storage", &state.startup_config.satellites.storage)?;

        let bucket = inst
            .env
            .get("media_bucket")
            .cloned()
            .unwrap_or_else(|| format!("{}-media", &inst.name));

        let hostname = inst
            .env
            .get("media_host")
            .cloned()
            .or(inst.env.get("S3_HOSTNAME").cloned())
            .unwrap_or_else(|| sat.host.clone());

        let mut proxy_host = inst
            .env
            .get("media_proxy")
            .cloned()
            .or_else(|| sat.proxy_address.clone())
            .unwrap_or_else(|| sat.proxy_host.to_owned());

        // media_proxy and proxy_address can contain a "{inst}" placeholder
        proxy_host = proxy_host.replace("{inst}", &inst.name);

        let shared_credentials = sat.shared_key_id.clone().and_then(|id| {
            sat.shared_secret
                .clone()
                .map(|sec| S3Credentials::new(id, sec))
        });
        let instance_credentials = {
            if let Some(config) = &state.startup_config.vault {
                let key_id = crate::secrets::query(config, &inst.name, "AWS_ACCESS_KEY_ID")?;
                let secret = crate::secrets::query(config, &inst.name, "AWS_SECRET_ACCESS_KEY")?;

                if let (Some(a), Some(b)) = (key_id, secret) {
                    log::debug!("using instance credentials ({a})");
                    Some(S3Credentials::new(a, b))
                } else {
                    log::debug!("using shared credentials");
                    None
                }
            } else {
                None
            }
        };

        state.mut_instance(&inst.name, |i| {
            // hostname for the s3 endpoint, used as a backend proxy URL
            // {protocol}://{storage_host}/{instance bucket}/{request path}
            i.status.storage_host = Some(hostname.clone());
            // this one is the bare name on which to serve files
            // (equivalent to the 'cloudfront hostname')
            i.status.storage_proxy = Some(proxy_host.clone());

            i.locally_updated = true;
        });

        Ok(Self {
            bucket,
            protocol: sat.protocol.to_owned(),
            host_address: hostname,
            proxy_address: proxy_host,
            credentials: instance_credentials
                .clone()
                .or_else(|| shared_credentials.clone())
                .context("no s3 credentials available")?,
            region: sat.region.clone(),
            shared_credentials,
            instance_credentials,
        })
    }

    // in here : create bucket, create subkeys, fix file permissions?, manage export buckets?
}
