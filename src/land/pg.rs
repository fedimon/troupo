use crate::config::{InstanceConfig, Species};
use crate::env_builder::EnvBuilder;
use crate::land::*;
use crate::state::AppState;
use anyhow::{anyhow, Context, Result};

pub struct DbEnvKeySet {
    pub user: &'static str,
    pub pass: &'static str,
    pub name: &'static str,
}

pub fn get_pg_keys(species: &Species) -> DbEnvKeySet {
    match species {
        crate::config::Species::Mastodon => DbEnvKeySet {
            user: "DB_USER",
            pass: "DB_PASS",
            name: "DB_NAME",
        },
        crate::config::Species::Pixelfed => DbEnvKeySet {
            user: "DB_USERNAME",
            pass: "DB_PASSWORD",
            name: "DB_DATABASE",
        },
        crate::config::Species::GoToSocial => DbEnvKeySet {
            user: "DB_USER",
            pass: "DB_PASS",
            name: "DB_NAME",
        },
    }
}

use crate::env_builder::keygen_password;

#[derive(Debug)]
pub struct PgConfig {
    pub dbname: String,
    pub username: String,
    pub password: String,
    pub admin_username: String,
    pub admin_password: Option<String>,
    pub host: String,
    pub port: u16,
}
impl PgConfig {
    pub fn from_env_builder(ctx: &EnvBuilder) -> Result<Self> {
        Self::from_inst(&ctx.state, &ctx.instance_config)
    }
    pub fn from_inst(state: &AppState, inst: &InstanceConfig) -> Result<Self> {
        let keys = get_pg_keys(&inst.species);
        let db_config =
            inst.satellite_conf("postgres", &state.startup_config.satellites.postgres)?;

        Ok(Self {
            dbname: format!("maas_{}", inst.name),
            username: inst
                .env
                .get(keys.user.clone())
                .map(|v| v.to_owned())
                .unwrap_or_else(|| format!("maas_{}", inst.name)),
            password: inst
                .greate_secret(state, keys.pass, keygen_password)
                .context("inst pg password")?,
            admin_password: db_config.admin_password.clone(),
            admin_username: db_config.admin_username.clone(),
            host: db_config.host.clone(),
            port: db_config.port.unwrap_or(5432),
        })
    }
    /// makes the current confiog use pgbouncer if available
    pub fn with_bouncer(mut self, state: &AppState) -> Self {
        if let Some(c) = &state.startup_config.pgbouncer {
            self.port = c.port;
            self.host = c.host.clone();
        }
        self
    }
}

pub fn setup<T: Actor>(conf: &PgConfig, changes: &mut ChangeSet) -> Result<()> {
    let db_user = &conf.username;
    let db_pass = &conf.password;
    let db_name = &conf.dbname;
    T::with_db_admin(conf, |client| {
        log::debug!("pg: create or update role {db_user}");
        let user_exist: i64 = client
            .query_one(
                "SELECT count(*) FROM pg_catalog.pg_user WHERE usename = $1",
                &[&db_user],
            )?
            .get(0);
        changes.push(
            if user_exist > 0 {
                client.execute(
                    &format!("ALTER ROLE \"{db_user}\" WITH PASSWORD '{db_pass}'"),
                    &[],
                )?;
                Changes::changed("updated")
            } else {
                client.execute(
                    &format!("CREATE ROLE \"{db_user}\" LOGIN PASSWORD '{db_pass}'"),
                    &[],
                )?;
                Changes::changed("updated")
            },
            "pg role",
        );

        let db_exist: i64 = client
            .query_one(
                "SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname = $1",
                &[&db_name],
            )?
            .get(0);
        changes.push(
            if db_exist == 0 {
                log::debug!("pg: create database {db_name}");
                client.execute(&format!("GRANT \"{db_user}\" TO CURRENT_USER;"), &[])?;
                client.execute(
                    &format!(
                        "CREATE DATABASE \"{db_name}\" WITH OWNER \"{db_user}\" ENCODING 'UTF8'"
                    ),
                    &[],
                )?;
                Changes::changed("database created")
            } else {
                Changes::Pass
            },
            "pg database",
        );

        Ok(())
    })?;

    Ok(())
}
