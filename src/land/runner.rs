use crate::config::{InstanceConfig, Species};
use crate::state::AppState;
use crate::task::{Task, TaskError, TaskResult};
use anyhow::{anyhow, Result};
use std::cell::RefCell;
use std::sync::Arc;
use std::time::Duration;
use std::{collections::HashMap, process::Command};

/// utility to run a subprocess (for a task & instance) as a systemd
/// unit thru systemd-run.
/// main use being keeping it across daemon restarts
pub struct TaskRunner<'a> {
    state: &'a Arc<AppState>,
    task: &'a Task,
    pub inst: InstanceConfig,
    user_id: usize,
    unit_name: String,
    directory: String,
    last_cmd: RefCell<String>,
}
impl<'a> TaskRunner<'a> {
    /// run any task under this user
    pub fn run(&self, add_args: &[&str]) -> Result<usize> {
        let mut args: Vec<String> = Vec::new();

        self.state.mut_task(&self.task.id, |i| {
            i.subcommand_id += 1;
        });
        self.last_cmd.replace(
            add_args
                .iter()
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .join(" "),
        );

        if self.user_id == 0 {
            log::debug!("run.root: {:?}", add_args);
            args.extend_from_slice(&[
                "--no-block".into(),
                "--remain-after-exit".into(),
                format!("--unit={}", self.full_unit_name()),
            ]);
        } else {
            log::debug!("run.inst: {:?}", add_args);
            args.extend_from_slice(&[
                "--no-block".into(),
                "--remain-after-exit".into(),
                format!("--uid={}", self.user_id),
                format!("--unit={}", self.full_unit_name()),
                format!(
                    "--property=EnvironmentFile=/etc/troupo/env/{}",
                    self.task.instance
                ),
                format!("--working-directory={}", self.directory),
            ]);
        }
        args.extend(add_args.into_iter().map(|x| (*x).to_owned()));

        self._run("systemd-run", &args)
    }

    pub fn check_run(&self, add_args: &[&str], expected_code: usize) -> TaskResult {
        let code = self.run(add_args)?;

        if code != expected_code {
            return Err(TaskError::command(code, self.output()?));
        }

        Ok(serde_json::json!({
            "output": self.output().ok(),
            "code": code,
        }))
    }

    pub fn as_user(state: &'a Arc<AppState>, task: &'a Task) -> Result<Self> {
        let inst = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("missing instance"))?;
        let username = get_username(&task.instance, &inst);
        let mut directory = format!("/home/{username}/");
        if inst.species == Species::Mastodon {
            let version_path = state
                .startup_config
                .versions
                .mastodon
                .get(&inst.version)
                .ok_or(anyhow!("version not found locally"))?;
            directory = version_path.clone();
        }
        Ok(Self {
            user_id: get_uid(&username)?,
            directory,
            state,
            task,
            inst,
            unit_name: format!("run-task-{}", task.id.as_simple()),
            last_cmd: RefCell::new(String::new()),
        })
    }

    pub fn as_root(state: &'a Arc<AppState>, task: &'a Task) -> Result<Self> {
        let inst = state
            .get_instance(&task.instance)
            .ok_or(anyhow!("missing instance"))?;
        Ok(Self {
            user_id: 0,
            directory: format!("/root"),
            state,
            task,
            inst,
            unit_name: format!("run-task-{}", task.id.as_simple()),
            last_cmd: RefCell::new(String::new()),
        })
    }

    pub fn with_directory(&mut self, d: &str) -> &mut Self {
        self.directory = d.to_string();
        self
    }

    fn full_unit_name(&self) -> String {
        // assume the task still exist, get the latest state
        let id = self
            .state
            .get_task(self.task.id)
            .map(|t| t.subcommand_id)
            .unwrap_or(0);
        format!("{}-{}", self.unit_name, id)
    }

    fn _run(&self, cmd: &str, args: &[String]) -> Result<usize> {
        log::trace!("running {:?}", args);
        log::trace!("in unit {}", self.full_unit_name());
        let mut proc = Command::new(cmd);
        proc.args(args);
        let out = proc.output()?;
        let stderr = String::from_utf8_lossy(&out.stderr);
        if !out.status.success() {
            return Err(anyhow!("subcommand failed with {}: {}", out.status, stderr));
        }

        self.wait()?;
        let code = self.exit_code()?;
        self.cleanup()?;
        log::trace!("exit code: {:?}", code);
        log::trace!("cleaned up unit {}", self.full_unit_name());

        Ok(code)
    }
    /// calls journalctl to get unit output
    pub fn output(&self) -> Result<String> {
        Command::new("journalctl")
            .args(&[
                "--no-pager",
                "-q",
                "--output=cat",
                "-n 200",
                &format!("--unit={}", self.full_unit_name()),
            ])
            .output()
            .map_err(|e| anyhow!("`journalctl` failed: {}", e))
            .map(|output| String::from_utf8_lossy(&output.stdout).into_owned())
    }
    /// calls systemctl show to get exit code
    pub fn exit_code(&self) -> Result<usize> {
        let output = Command::new("systemctl")
            .args(&["show", &self.full_unit_name()])
            .output()
            .map_err(|e| anyhow!("`systemctl show` failed: {}", e))
            .map(|output| String::from_utf8_lossy(&output.stdout).into_owned())?;

        let kv: HashMap<&str, &str> = output
            .split('\n')
            .filter_map(|line| line.split_once('='))
            .collect();

        let exitcode: Option<usize> = kv.get("ExecMainStatus").and_then(|x| x.trim().parse().ok());

        exitcode.ok_or(anyhow!("no exit code found in systemctl show output"))
    }
    pub fn _unit_status(&self) -> Result<(Option<String>, Option<String>)> {
        let output = Command::new("systemctl")
            .args(&["show", &self.full_unit_name()])
            .output()
            .map_err(|e| anyhow!("`systemctl show` failed: {}", e))
            .map(|output| String::from_utf8_lossy(&output.stdout).into_owned())?;

        let kv: HashMap<&str, &str> = output
            .split('\n')
            .filter_map(|line| line.split_once('='))
            .collect();
        let state: Option<String> = kv.get("ActiveState").cloned().map(String::from);
        let substate: Option<String> = kv.get("SubState").cloned().map(String::from);
        Ok((state, substate))
    }
    pub fn _sync_output(&self) -> Result<()> {
        let output = self.output()?;
        self.state.mut_task(&self.task.id, |i| {
            let id = i.subcommand_id;
            if i.output.len() <= id {
                i.output
                    .resize_with(id + 1, || (String::new(), String::new()));
            }
            i.output[id] = (self.last_cmd.borrow().clone(), output);
            i.locally_updated = true;
        });

        Ok(())
    }
    pub fn wait(&self) -> Result<()> {
        let period = Duration::from_secs(3);
        let mut times = (vec![
            Duration::from_millis(33),
            Duration::from_millis(125),
            Duration::from_millis(250),
            Duration::from_millis(500),
            Duration::from_secs(1),
            Duration::from_secs(3),
        ])
        .into_iter()
        .chain(std::iter::repeat(period));
        loop {
            let (state, substate) = self._unit_status()?;
            self._sync_output();
            match state.as_ref().map(|s| s.as_str()) {
                Some("inactive") => return Ok(()),
                Some("failed") => return Ok(()),
                Some("active") if substate.as_ref().map(|s| s.as_str()) == Some("exited") => {
                    return Ok(())
                }
                Some(_) => {}
                None => {}
            }

            std::thread::sleep(times.next().unwrap());
        }
    }
    pub fn cleanup(&self) -> Result<()> {
        let wildcard = format!("{}-*", self.unit_name);
        let reset_output = Command::new("systemctl")
            .args(&["reset-failed", &wildcard])
            .output()
            .map_err(|e| anyhow!("`systemctl reset-failed` failed: {}", e))
            .map(|output| String::from_utf8_lossy(&output.stdout).into_owned());
        let stop_output = Command::new("systemctl")
            .args(&["stop", &wildcard])
            .output()
            .map_err(|e| anyhow!("`systemctl stop` failed: {}", e))
            .map(|output| String::from_utf8_lossy(&output.stdout).into_owned());

        if reset_output.is_ok() || stop_output.is_ok() {
            return Ok(());
        }

        anyhow::bail!(
            "failed to clean up after {} :(\nreset-failed: {:?}\nstop: {:?}",
            self.full_unit_name(),
            reset_output,
            stop_output
        )
    }
}

pub fn get_username(inst_name: &str, inst: &InstanceConfig) -> String {
    match inst.species {
        Species::Mastodon => inst.version.to_owned(),
        Species::Pixelfed => format!("pix-{}", inst_name),
        Species::GoToSocial => format!("gts-{}", inst_name),
    }
}

pub fn get_uid(username: &str) -> Result<usize> {
    Command::new("id")
        .args(["-u", username])
        .output()
        .map_err(|e| anyhow!("`id {}` failed: {}", username, e))
        .and_then(|output| {
            let stdout = String::from_utf8_lossy(&output.stdout);
            let stderr = String::from_utf8_lossy(&output.stderr);
            let i: usize = stdout.as_ref().trim().parse().map_err(|e| {
                anyhow!("failed to parse `id {username}`: {e} (err: {stderr:?}, out: {stdout:?})")
            })?;
            Ok(i)
        })
}
