// pgbounce her?? i've never getting tired of these

use super::Actor;
use super::Changes;
use crate::land::*;
use crate::state::AppState;
use crate::task::{Task, TaskResult};

use anyhow::{bail, Context, Result};
use std::path::Path;
use std::sync::Arc;

pub fn setup<T: Actor>(
    state: &Arc<AppState>,
    task: &Task,
    changes: &mut ChangeSet,
    pool_mode: &'static str,
) -> TaskResult<()> {
    if state.startup_config.pgbouncer.is_some() {
        changes.push(
            crate::land::pgbouncer::update_users::<T>(state)?,
            "pgbouncer users",
        );
        changes.push(
            crate::land::pgbouncer::update_config::<T>(state, pool_mode)?,
            "pgbouncer config",
        );
        T::run_as_root(
            state,
            task,
            &[
                "systemctl".into(),
                "reload".into(),
                "pgbouncer.service".into(),
            ],
        )?;
    }

    Ok(())
}

pub fn update_users<T: Actor>(state: &Arc<AppState>) -> Result<Changes> {
    let path = Path::new("/etc/pgbouncer/userlist.txt");

    let mut content = String::new();

    let mut instances = state.get_instances();
    instances.sort();

    for key in &instances {
        let ctx = match crate::env_builder::EnvBuilder::new(state, &key) {
            Ok(v) => v,
            Err(_e) => continue,
        };
        let config = match ctx.get_sat_config("postgres", &ctx.local_config.satellites.postgres) {
            Ok(v) => v,
            Err(_e) => continue,
        };

        let keys = ctx.get_pg_keys();

        let user_name = crate::env_builder::add_prefix(&ctx.instance, &config.user_prefix);
        let password = match ctx.get_secret(&keys.pass)? {
            Some(v) => v,
            None => continue,
        };

        content.push_str(&format!("\"{user_name}\" \"{password}\"\n"));
    }

    let c = T::set_text(path, &content)
        .with_context(|| format!("writing writing pgbouncer userlist"))?;

    Ok(c)
}

pub fn update_config<T: Actor>(state: &Arc<AppState>, pool_mode: &'static str) -> Result<Changes> {
    let path = Path::new("/etc/pgbouncer/pgbouncer.ini");

    let mut content = String::new();

    let pgb_config = match &state.startup_config.pgbouncer {
        Some(pgb) => pgb,
        None => bail!("no pgbouncer config found"),
    };
    let bouncer_port = pgb_config.port;

    let mut instances = state.get_instances();
    instances.sort();

    content.push_str("[databases]\n");
    for key in &instances {
        let ctx = match crate::env_builder::EnvBuilder::new(state, &key) {
            Ok(v) => v,
            Err(_e) => continue,
        };
        let config = match ctx.get_sat_config("postgres", &ctx.local_config.satellites.postgres) {
            Ok(v) => v,
            Err(_e) => continue,
        };
        let db_name = crate::env_builder::add_prefix(&ctx.instance, &config.db_prefix);
        let host = &config.host;
        let pg_port = config.port.unwrap_or(5432);
        content.push_str(&format!("{db_name} = host={host} port={pg_port}\n"));
    }

    content.push_str(&format!(
        "
[pgbouncer]
logfile = /var/log/postgresql/pgbouncer.log
pidfile = /var/run/postgresql/pgbouncer.pid

listen_addr = 127.0.0.1
listen_port = {bouncer_port}
auth_type = plain
auth_file = /etc/pgbouncer/userlist.txt

pool_mode = {pool_mode}
server_reset_query = DISCARD ALL
server_check_query = select 1
server_check_delay = 30

max_client_conn = 800
default_pool_size = 100
"
    ));

    let c = T::set_text(path, &content)
        .with_context(|| format!("writing writing pgbouncer userlist"))?;

    Ok(c)
}
