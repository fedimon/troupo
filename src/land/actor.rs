use anyhow::{anyhow, Context, Result};
use postgres::Client;
use serde::Serialize;
use similar::TextDiff;
use std::io::ErrorKind;
use std::path::Path;
use std::sync::Arc;

use super::pg::PgConfig;
use crate::state::AppState;
use crate::task::{Task, TaskError, TaskResult};

#[derive(Debug, PartialEq, Serialize)]
#[serde(tag = "_state")]
pub enum Changes {
    Pass,
    Changed { summary: String },
    Diff { name: String, diff: String },
}
impl Changes {
    pub fn diff(name: &str, diff: String) -> Self {
        Self::Diff {
            name: name.into(),
            diff,
        }
    }
    pub fn changed(s: &str) -> Self {
        Self::Changed { summary: s.into() }
    }
}
#[derive(Debug, PartialEq, Serialize)]
pub struct NamedChanges {
    #[serde(rename = "_name")]
    name: String,
    #[serde(flatten)]
    change: Changes,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct ChangeSet {
    items: Vec<NamedChanges>,
}
impl ChangeSet {
    pub fn new() -> Self {
        Self { items: Vec::new() }
    }
    pub fn push(&mut self, change: Changes, title: &str) {
        self.items.push(NamedChanges {
            name: title.to_owned(),
            change: change,
        });
    }
}

/// a thing that Acts on a System
pub trait Actor {
    fn set_text(path: &Path, content: &str) -> Result<Changes> {
        Self::set_file(path, content.as_bytes())
    }
    fn set_file(path: &Path, content: &[u8]) -> Result<Changes>;

    fn with_db_admin<F: FnMut(&mut Client) -> Result<()>>(ctx: &PgConfig, f: F) -> Result<()>;

    //fn run_as(state: &Arc<AppState>, task: &Task, user: &str, args: &[String]) -> TaskResult<String>;
    fn run_as_root(state: &Arc<AppState>, task: &Task, args: &[&str]) -> TaskResult<String>;
    fn run_as_inst(state: &Arc<AppState>, task: &Task, args: &[&str]) -> TaskResult<String>;
    fn run_as_inst_in(
        state: &Arc<AppState>,
        task: &Task,
        dir: &str,
        args: &[&str],
    ) -> TaskResult<String>;

    fn svc_is_enabled(state: &Arc<AppState>, task: &Task, unit: &str) -> TaskResult<bool> {
        let sub = super::runner::TaskRunner::as_root(state, task)?;
        let code = sub.run(&["systemctl".into(), "is-enabled".into(), unit.into()])?;
        Ok(code == 0)
    }

    fn mkdir(path: &Path) -> Result<Changes>;
    fn delete(path: &Path) -> Result<Changes>;
}

/// this one will compare new values with the local system and not
/// write any change
pub struct LocalCheckActor;
impl Actor for LocalCheckActor {
    fn set_text(path: &Path, content: &str) -> Result<Changes> {
        let existing = match std::fs::read_to_string(path) {
            Ok(f) => f,
            Err(e) if e.kind() == ErrorKind::NotFound => {
                return Ok(Changes::changed("[file does not exist]"))
            }
            Err(e) => return Err(anyhow!("failed to read {:?}: {}", path, e)),
        };
        if existing == content {
            return Ok(Changes::Pass);
        }

        let r = TextDiff::from_lines(existing.as_str(), content)
            .unified_diff()
            .to_string();
        return Ok(Changes::diff(&format!("{path:?}"), r));
    }

    fn set_file(path: &Path, content: &[u8]) -> Result<Changes> {
        let existing = match std::fs::read(path) {
            Ok(f) => f,
            Err(e) if e.kind() == ErrorKind::NotFound => {
                return Ok(Changes::changed("[file does not exist]"))
            }
            Err(e) => return Err(anyhow!("failed to read {:?}: {}", path, e)),
        };
        if existing == content {
            return Ok(Changes::Pass);
        }
        return Ok(Changes::changed("[binary files differ]"));
    }
    fn mkdir(path: &Path) -> Result<Changes> {
        if path.exists() {
            return Ok(Changes::Pass);
        }
        return Ok(Changes::changed("[created]"));
    }
    fn delete(path: &Path) -> Result<Changes> {
        if !path.exists() {
            return Ok(Changes::Pass);
        }
        return Ok(Changes::changed("[file exists]"));
    }
    fn with_db_admin<F: FnMut(&mut Client) -> Result<()>>(_ctx: &PgConfig, _f: F) -> Result<()> {
        // nothing is done here
        Ok(())
    }
    fn run_as_inst(state: &Arc<AppState>, task: &Task, args: &[&str]) -> TaskResult<String> {
        Ok("".to_owned())
    }

    fn run_as_root(_state: &Arc<AppState>, _task: &Task, _args: &[&str]) -> TaskResult<String> {
        Ok("".to_owned())
    }
    fn run_as_inst_in(
        state: &Arc<AppState>,
        task: &Task,
        dir: &str,
        args: &[&str],
    ) -> TaskResult<String> {
        Ok("".to_owned())
    }
}

pub struct LocalWriteActor;
impl Actor for LocalWriteActor {
    fn set_text(path: &Path, content: &str) -> Result<Changes> {
        let existing = match std::fs::read_to_string(path) {
            Ok(f) => f,
            Err(e) if e.kind() == ErrorKind::NotFound => String::new(),
            Err(e) => return Err(anyhow!("failed to read {:?}: {}", path, e)),
        };
        if existing == content && content != "" {
            return Ok(Changes::Pass);
        }

        let r = TextDiff::from_lines(existing.as_str(), content)
            .unified_diff()
            .to_string();

        // write the file
        std::fs::write(path, content).with_context(|| format!("failed to write {:?}", path))?;

        Ok(Changes::diff(&format!("{path:?}"), r))
    }
    fn set_file(path: &Path, content: &[u8]) -> Result<Changes> {
        // let errors and validated files through
        let check_result = match LocalCheckActor::set_file(path, content) {
            Ok(c) => c,
            Err(e) => return Err(e),
        };
        if check_result == Changes::Pass {
            return Ok(Changes::Pass);
        }

        // write the file
        std::fs::write(path, content).with_context(|| format!("failed to write {:?}", path))?;

        Ok(check_result)
    }
    fn mkdir(path: &Path) -> Result<Changes> {
        if path.exists() {
            return Ok(Changes::Pass);
        }
        log::debug!("creating dir {path:?}");
        std::fs::create_dir_all(path)?;
        return Ok(Changes::changed("[created]"));
    }
    fn delete(path: &Path) -> Result<Changes> {
        // let errors and validated files through
        let check_result = match LocalCheckActor::delete(path) {
            Ok(c) => c,
            Err(e) => return Err(e),
        };
        if check_result == Changes::Pass {
            return Ok(Changes::Pass);
        }

        log::debug!("deleting path {path:?}");
        std::fs::remove_file(path).with_context(|| format!("failed to remove {:?}", path))?;
        Ok(Changes::changed("[file deleted]"))
    }
    fn with_db_admin<F: FnMut(&mut Client) -> Result<()>>(
        db_config: &PgConfig,
        mut f: F,
    ) -> Result<()> {
        let mut cs = format!(
            "host={} user={} dbname=postgres",
            db_config.host, db_config.admin_username,
        );
        if let Some(p) = &db_config.admin_password {
            cs += &format!(" password='{}'", p);
        }
        let mut client = postgres::Client::connect(&cs, postgres::NoTls).with_context(|| {
            format!(
                "failed to connect to {} for pg admin access",
                db_config.host
            )
        })?;

        f(&mut client)?;

        Ok(())
    }
    fn run_as_inst(state: &Arc<AppState>, task: &Task, args: &[&str]) -> TaskResult<String> {
        let sub = super::runner::TaskRunner::as_user(state, task)?;
        let code = sub.run(args)?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }
        Ok(output)
    }
    fn run_as_inst_in(
        state: &Arc<AppState>,
        task: &Task,
        dir: &str,
        args: &[&str],
    ) -> TaskResult<String> {
        let mut sub = super::runner::TaskRunner::as_user(state, task)?;
        sub.with_directory(dir);
        let code = sub.run(args)?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }
        Ok(output)
    }
    fn run_as_root(state: &Arc<AppState>, task: &Task, args: &[&str]) -> TaskResult<String> {
        let sub = super::runner::TaskRunner::as_root(state, task)?;
        let code = sub.run(args)?;
        let output = sub.output()?;
        if code != 0 {
            return Err(TaskError::command(code, output));
        }
        Ok(output)
    }
}
