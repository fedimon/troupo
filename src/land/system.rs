use crate::env_builder::EnvBuilder;
use crate::land::*;
use crate::land::{get_bin_path, serialize_env};
use crate::state::AppState;
use crate::task::def::TaskResult;
use crate::task::Task;
use anyhow::{anyhow, Context, Result};
use std::path::PathBuf;
use std::sync::Arc;

pub fn setup_user<T: Actor>(state: &Arc<AppState>, task: &Task, username: &str) -> Result<Changes> {
    let r = T::run_as_root(
        state,
        task,
        &["useradd", "--create-home", "--shell", "/bin/bash", username],
    );
    Ok(match r {
        Ok((_)) => Changes::changed("user created"),
        Err(te) if te.code == 9 => Changes::Pass,
        Err(te) => return Err(anyhow!("useradd returned code {}", te.code)),
    })
}
