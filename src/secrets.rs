use crate::{config::VaultConfig, state::AppState};
use anyhow::{bail, Result};
use reqwest::blocking::Client;
use reqwest::{header, StatusCode};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::time::Duration;

#[derive(Serialize, Deserialize)]
struct SecretData {
    secret_value: String,
}

#[derive(Serialize, Deserialize)]
struct VaultData {
    data: SecretData,
    metadata: Value,
}

#[derive(Serialize, Deserialize)]
struct GetResponse {
    data: VaultData,
}

#[derive(Serialize, Deserialize)]
struct PostRequestBody {
    data: SecretData,
}

fn build_client(config: &VaultConfig) -> Result<Client> {
    let auth = format!("Bearer {}", config.token);
    let mut headers = header::HeaderMap::new();
    headers.insert(
        "Authorization",
        header::HeaderValue::from_str(&auth).unwrap(),
    );

    let client = reqwest::blocking::Client::builder()
        .timeout(Duration::from_secs(5))
        .default_headers(headers)
        .build()?;

    Ok(client)
}

pub fn save(state: &AppState, inst: &str, key: &str, value: &str) -> Result<()> {
    let config = match &state.startup_config.vault {
        Some(config) => config,
        None => {
            log::warn!("vault not enabled. saving secret as param");
            state.mut_instance(inst, |inst| {
                inst.env.insert(key.to_owned(), value.to_owned());
            });
            return Ok(());
        }
    };

    let client = build_client(config)?;

    let r = client
        .post(format!(
            "{}/v1/secret/data/troupo/instance/{inst}/param/{key}",
            config.url
        ))
        .json(&PostRequestBody {
            data: SecretData {
                secret_value: value.to_owned(),
            },
        })
        .send()?;

    if r.status() != StatusCode::OK {
        bail!(
            "failed to save secret {key} in vault: code={:?} data={:?}",
            r.status(),
            r.text()
        )
    }

    Ok(())
}

pub fn query(config: &VaultConfig, inst: &str, key: &str) -> Result<Option<String>> {
    let client = build_client(config)?;

    let r = client
        .get(format!(
            "{}/v1/secret/data/troupo/instance/{inst}/param/{key}",
            config.url
        ))
        .send()?;

    if r.status() == StatusCode::NOT_FOUND {
        log::trace!("querying instance/{inst}/param/{key} [missing]");
        return Ok(None);
    }

    if r.status() != StatusCode::OK {
        bail!(
            "failed to fetch secret {key} in vault: code={:?} data={:?}",
            r.status(),
            r.text()
        )
    }
    let r: GetResponse = r.json()?;
    log::trace!("querying instance/{inst}/param/{key} [value found]");

    Ok(Some(r.data.data.secret_value))
}
