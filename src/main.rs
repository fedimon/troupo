#[macro_use]
extern crate rocket;
use clap::{FromArgMatches as _, Subcommand};
use config::StartupConfig;
use std::sync::Arc;
use tokio::sync::mpsc;

pub mod config;
pub mod env_builder;
pub mod land;
pub mod secrets;
pub mod species;
pub mod state;
pub mod task;
pub mod web;

const GIT_VERSION: &str = git_version::git_describe!("--always", "--dirty=-modified");

pub fn version() -> String {
    format!("{}-{}", env!("CARGO_PKG_VERSION"), GIT_VERSION)
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// run the troupo server
    Serve {
        #[clap(long = "port", default_value = "33399")]
        port: u16,
        #[clap(long = "bind", default_value = "127.0.0.1")]
        bind: std::net::IpAddr,
    },

    /// create default configuration file
    DefaultConf {},

    /// print detected local configuration
    #[clap(arg_required_else_help = true)]
    Inspect {},
}

#[rocket::main]
async fn main() -> anyhow::Result<()> {
    // TODO: default to INFO
    let version = version();

    env_logger::init();

    let cli = clap::Command::new("troupo")
        // log clap 4 requires version to be 'static again??
        // im not opening an issue for this
        .version(Box::leak(Box::new(version)).as_str())
        .arg_required_else_help(true)
        .about("mastodon herding tool");
    let cli = Commands::augment_subcommands(cli);
    let args = cli.get_matches();

    let (task_sender, task_receiver) = mpsc::channel(32);

    match Commands::from_arg_matches(&args)? {
        Commands::Serve { port, bind } => {
            let config = match config::StartupConfig::get() {
                Ok(c) => c,
                Err(e) => {
                    log::error!("failed to load startup config: {}", e);
                    return Err(anyhow::anyhow!("failed to load startup config"));
                }
            };
            let state = Arc::new(state::AppState::open(config, task_sender));

            task::exec::boot(&state, task_receiver);
            task::registry::spawn_sync_tasks(state.clone());

            match web::rocket::rocket(state, port, bind).launch().await {
                Ok(_r) => {}
                Err(e) => {
                    log::error!("failed to start web server: {}", e);
                    return Ok(());
                }
            };
        }
        Commands::Inspect {} => {
            //
        }
        Commands::DefaultConf {} => {
            let c = config::StartupConfig::default();
            println!("{}", toml::to_string_pretty(&c).unwrap());
        }
    }

    Ok(())
}
