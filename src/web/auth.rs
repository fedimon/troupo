use std::sync::Arc;

use rocket::outcome::Outcome;
use rocket::request::{self, FromRequest, Request};
use rocket::State;

use crate::state::AppState;

pub struct ApiAuth {
    key: String,
}

// i cant seem to find a way to import this from rocket::outcome
macro_rules! try_outcome {
    ($expr:expr $(,)?) => {
        match $expr {
            rocket::outcome::Outcome::Success(val) => val,
            rocket::outcome::Outcome::Failure(e) => {
                return rocket::outcome::Outcome::Failure(::std::convert::From::from(e))
            }
            rocket::outcome::Outcome::Forward(f) => {
                return rocket::outcome::Outcome::Forward(::std::convert::From::from(f))
            }
        }
    };
}

fn match_key(state: &Arc<AppState>, header: &str) -> Option<ApiAuth> {
    log::trace!("parsing auth header: {header}");
    let (method, arg) = header.split_once(' ')?;
    let method = method.to_lowercase();

    match method.as_str() {
        "bearer" => {
            let token = arg.trim();
            log::trace!("with bearer {token:?}");
            state.startup_config.api_keys.get(token)
        }
        "basic" => {
            let token = base64::decode(arg.trim())
                .ok()
                .map(|v| String::from_utf8_lossy(&v).into_owned())?;
            let token = token.trim().trim_matches(':');
            log::trace!("with basic token {token:?}");

            state.startup_config.api_keys.get(token)
        }
        _ => None,
    }
    .map(|id| ApiAuth { key: id.to_owned() })
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for ApiAuth {
    type Error = String;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let state = try_outcome!(req
            .guard::<&State<Arc<AppState>>>()
            .await
            .map_failure(|(status, _)| (status, "state guard failed; skipping auth".to_owned())));

        log::trace!("headers: {:?}", req.headers());
        let key = req
            .headers()
            .get_one("authorization")
            .and_then(|header| match_key(state, header));

        match key {
            Some(key) => {
                log::debug!("identified as {}", key.key);
                Outcome::Success(key)
            }
            None => Outcome::Failure((
                rocket::http::Status::Forbidden,
                "Authentication failed".to_owned(),
            )),
        }
    }
}
