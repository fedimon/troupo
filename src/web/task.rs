use chrono::Utc;
use rocket::form::Form;
use rocket::http::Status;
use rocket::serde::json::Json;

use rocket::serde::uuid::Uuid;
use rocket::State;

use super::auth::ApiAuth;
use super::types::*;
use crate::task::{Task, TaskState};

#[get("/task/<id>")]
pub fn get_task(state: &State<WebState>, _key: ApiAuth, id: Uuid) -> Option<Json<APIResponse>> {
    let task = state.get_task(id)?;
    Some(Json(APIResponse::Task(task.into())))
}

#[get("/task")]
pub fn get_task_list(
    state: &State<WebState>,
    _key: ApiAuth,
) -> Option<Json<APIResponse<TaskStatus>>> {
    let tasks: Vec<TaskStatus> = state
        .get_tasks()
        .unwrap_or_default()
        .into_iter()
        .map(TaskStatus::from)
        .collect();

    Some(Json(APIResponse::List(List::new(tasks))))
}

#[put("/task/<id>", data = "<form>")]
pub async fn put_task(
    state: &State<WebState>,
    _key: ApiAuth,
    id: Uuid,
    form: Form<NewTask>,
) -> Result<Json<APIResponse>, Status> {
    // ensure the instance exists
    let _inst_config = state.get_instance(&form.instance).ok_or_else(|| {
        log::debug!("instance not found for new task: {}", form.instance);
        Status::NotFound
    })?;

    let existing_task = state.get_task(id);
    if let Some(e) = existing_task {
        // no allowed changes. we just compare and return it as is
        // or fail right here why not
        if e.task_type != form.task_type {
            return Err(Status::Conflict);
        }
        //if e.parameters != form.parameters {
        //    return Err(Status::Conflict);
        //}
        // TODO
        return Ok(Json(APIResponse::Task(TaskStatus::from(e))));
    }

    let task = Task {
        id: id,
        instance: form.instance.to_owned(),
        task_type: form.task_type,
        state: TaskState::Pending,
        create_date: Utc::now(),
        update_date: None,
        parameters: form
            .parameters
            .iter()
            .map(|(k, v)| (k.to_owned(), serde_json::Value::from(v.as_str())))
            .collect(),
        result: None,
        output: Default::default(),
        subcommand_id: 0,
        locally_updated: false,
    };
    state.add_task(&task);
    crate::state::on_task_add(state.inner().clone(), &task).await;

    Ok(Json(APIResponse::Task(TaskStatus::from(task))))
}

#[get("/instance/<name>/task")]
pub fn get_instance_task_list(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
) -> Option<Json<APIResponse<TaskStatus>>> {
    let _inst_config = state.get_instance(name)?;

    let tasks: Vec<TaskStatus> = state
        .get_instance_tasks(name)
        .unwrap_or_default()
        .into_iter()
        .map(TaskStatus::from)
        .collect();

    Some(Json(APIResponse::List(List::new(tasks))))
}

#[post("/instance/<name>/task", data = "<form>")]
pub async fn post_instance_task(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
    form: Form<NewInstanceTask>,
) -> Result<Json<APIResponse>, Status> {
    let _inst_config = state.get_instance(name).ok_or(Status::NotFound)?;

    let task = Task::new(name, form.task_type).with_params(
        form.parameters
            .iter()
            .map(|(k, v)| (k.to_owned(), serde_json::Value::from(v.as_str())))
            .collect(),
    );
    task.enqueue(state).await;

    Ok(Json(APIResponse::Task(TaskStatus::from(task))))
}
