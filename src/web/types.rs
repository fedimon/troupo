use rocket::serde::Serialize;
use serde::Deserialize;
use std::collections::HashMap;
use std::sync::Arc;

use crate::config::{InstanceConfig, InstanceStats, Species, WebScale, WorkerModel, WorkerScale};
use crate::state::AppState;
use crate::task::{TaskState, TaskType};

pub type WebState = Arc<AppState>;

#[derive(Serialize)]
pub struct List<T> {
    pub items: Vec<T>,
}
impl<T> List<T> {
    pub fn new(v: Vec<T>) -> Self {
        Self { items: v }
    }
}

#[derive(Serialize)]
#[serde(tag = "_type")]
#[serde(rename_all = "snake_case")]
pub enum APIResponse<U = ()> {
    Troupo(TroupoStatus),
    Task(TaskStatus),
    Instance(InstanceStatus),
    InstanceEnv(InstanceEnv),
    List(List<U>),
}

#[derive(Serialize)]
pub struct TroupoStatus {
    pub name: String,
    pub version: String,
    pub instances: HashMap<String, String>,
    pub total_stats: InstanceStats,
    pub available_satellites: HashMap<String, Vec<String>>,
    pub host: HostStatus,
}

#[derive(Serialize, Default)]
pub struct HostStatus {
    pub mem_allocated_mb: f32,
    pub mem_used_mb: f32,
    pub mem_total_mb: f32,

    pub mem_usage: f32,
    pub cpu_usage: f32,
}

#[derive(Serialize)]
pub struct InstanceStatus {
    pub name: String,
    pub domain: String,
    // config
    pub species: Species,
    pub version: String,
    pub local_id: u16,
    pub run_scheduler: bool,
    pub web_scale: WebScale,
    pub worker_scale: WorkerScale,
    pub env: HashMap<String, String>,
    /// map of sat id (pg, redis, ..) to their local identifier (default, host1, ..)
    pub satellites: HashMap<String, String>,
    pub stats: Option<InstanceStats>,
    pub status: crate::config::InstanceStatus,
}
impl InstanceStatus {
    pub fn new(config: InstanceConfig, name: &str) -> Self {
        Self {
            name: name.to_owned(),
            domain: config.domain,
            species: config.species,
            version: config.version,
            local_id: config.local_id,
            run_scheduler: config.run_scheduler,
            web_scale: config.web_scale,
            worker_scale: config.worker_scale,
            env: config.env,
            satellites: config.satellites,
            stats: config.stats,
            status: config.status,
        }
    }
}
#[derive(Serialize)]
pub struct InstanceEnv {
    pub key: String,
    pub value: Option<String>,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct TaskStatus {
    pub id: uuid::Uuid,
    pub instance_name: String,
    pub task_type: TaskType,
    pub state: TaskState,
    pub create_date: chrono::NaiveDateTime,
    pub update_date: Option<chrono::NaiveDateTime>,
    pub parameters: serde_json::Value,
    pub result: Option<serde_json::Value>,
    pub output: Vec<(String, String)>,
}
impl From<crate::task::Task> for TaskStatus {
    fn from(v: crate::task::Task) -> Self {
        Self {
            id: v.id,
            instance_name: v.instance.to_owned(),
            task_type: v.task_type,
            state: v.state,
            create_date: v.create_date.naive_utc(),
            update_date: v.update_date.map(|v| v.naive_utc()),
            result: v.result,
            output: v.output,
            parameters: serde_json::Value::Object(v.parameters.clone().into_iter().collect()),
        }
    }
}

#[derive(FromForm, Deserialize)]
pub struct InstancePut {
    pub local_serial_id: Option<u16>,
    pub species: crate::config::Species,
    pub version: Option<String>,
    pub domain: Option<String>,
    pub worker_scale: Option<NewWorkerScale>,
    pub web_scale: Option<NewWebScale>,
    pub run_scheduler: Option<bool>,

    #[field(default = HashMap::new())]
    pub env: HashMap<String, String>,

    #[field(default = HashMap::new())]
    pub satellites: HashMap<String, String>,

    #[field(default = HashMap::new())]
    pub secrets: HashMap<String, String>,
}
impl InstancePut {
    /// Apply fields specific to instance creation
    /// (in addition to apply)
    pub fn apply_create(&self, inst: &mut InstanceConfig) {
        if let Some(v) = self.local_serial_id {
            log::trace!("set .local_id to {}", v);
            inst.local_id = v;
        }
        inst.species = self.species;
    }
    /// Apply fields on edit
    pub fn apply(&self, inst: &mut InstanceConfig) {
        if let Some(s) = &self.version {
            log::trace!("set .version to {:?}", s);
            inst.version = s.to_owned();
        }
        if let Some(s) = &self.domain {
            log::trace!("set .domain to {:?}", s);
            inst.domain = s.to_owned();
        }
        for (key, val) in self.env.iter() {
            log::trace!("set .env.{} to {:?}", key, val);
            inst.env.insert(key.to_owned(), val.to_owned());
        }
        for (key, val) in self.satellites.iter() {
            log::trace!("set .satellites.{} to {:?}", key, val);
            inst.satellites.insert(key.to_owned(), val.to_owned());
        }
        for (key, val) in self.secrets.iter() {
            log::trace!("set .secrets.{} to {:?}", key, val);
            // save in vault?
            // TODO
            //inst.env.insert(key.to_owned(), val.to_owned());
        }
        if let Some(s) = &self.run_scheduler {
            log::trace!("set .run_scheduler to {:?}", s);
            inst.run_scheduler = *s;
        }
        if let Some(s) = &self.web_scale {
            log::trace!("set .web_scale to {:?}", s);
            s.apply(&mut inst.web_scale);
        }
        if let Some(s) = &self.worker_scale {
            log::trace!("set .worker_scale to {:?}", s);
            s.apply(&mut inst.worker_scale);
        }
    }
}

/// new task, bound to a known instance
#[derive(FromForm)]
pub struct NewInstanceTask {
    pub task_type: TaskType,
    #[field(default = HashMap::new())]
    pub parameters: HashMap<String, String>,
}

/// new task, all fields
#[derive(FromForm)]
pub struct NewTask {
    pub instance: String,
    pub task_type: TaskType,
    #[field(default = HashMap::new())]
    pub parameters: HashMap<String, String>,
}

#[derive(FromForm, Deserialize)]
pub struct InstanceEnvPut {
    pub value: String,
}

#[derive(FromForm, Deserialize, Debug)]
pub struct NewWebScale {
    pub mem_mb: Option<usize>,
    pub processes: Option<usize>,
    pub min_threads: Option<usize>,
    pub max_threads: Option<usize>,
}
impl NewWebScale {
    pub fn apply(&self, to: &mut WebScale) {
        if let Some(v) = self.mem_mb {
            to.mem_mb = v
        }
        if let Some(v) = self.processes {
            to.processes = v
        }
        if let Some(v) = self.min_threads {
            to.min_threads = v
        }
        if let Some(v) = self.max_threads {
            to.max_threads = v
        }
    }
}

#[derive(FromForm, Deserialize, Debug)]
pub struct NewWorkerScale {
    pub model: Option<WorkerModel>,
    pub mem_mb: Option<usize>,
    pub threads: Option<usize>,
}
impl NewWorkerScale {
    pub fn apply(&self, to: &mut WorkerScale) {
        if let Some(v) = &self.model {
            to.model = v.clone()
        }
        if let Some(v) = self.mem_mb {
            to.mem_mb = v
        }
        if let Some(v) = self.threads {
            to.threads = v
        }
    }
}
