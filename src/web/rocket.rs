use rocket::serde::json::Json;
use rocket::State;
use std::sync::Arc;

use super::auth::ApiAuth;
use super::types::*;
use super::{instance, task};
use crate::config::{InstanceStats, Species};
use crate::state::AppState;

use sysinfo::{CpuExt, SystemExt};

fn fetch_host_status(state: &State<WebState>) -> HostStatus {
    let mut host_system = match state.host_system.lock() {
        Ok(h) => h,
        Err(e) => return HostStatus::default(),
    };
    host_system.refresh_cpu();
    host_system.refresh_memory();
    host_system.refresh_disks();

    let used_mem = host_system.used_memory() as f32 / 1_000_000.0;
    let available_mem = host_system.available_memory() as f32 / 1_000_000.0;
    let total_mem = host_system.total_memory() as f32 / 1_000_000.0;

    let mut allocated_mem = 0;

    state.runtime_config.get_with(|c| {
        for (_, i) in &c.instances {
            allocated_mem += i.mem_allocation_mb();
        }
    });

    HostStatus {
        mem_used_mb: used_mem,
        mem_total_mb: total_mem,
        mem_allocated_mb: allocated_mem as f32,
        mem_usage: 1.0 - available_mem / total_mem,
        cpu_usage: host_system.global_cpu_info().cpu_usage() / 100.0,
    }
}

#[get("/")]
fn index(state: &State<WebState>, _key: ApiAuth) -> Json<APIResponse> {
    let stats: Vec<InstanceStats> = state
        .runtime_config
        .config
        .read()
        .instances
        .values()
        .filter_map(|i| i.stats.clone())
        .collect();
    let sats = state.startup_config.satellites.keys();
    let inst_list = state
        .runtime_config
        .config
        .read()
        .instances
        .keys()
        .map(|k| (k.to_owned(), format!("/instance/{}", k)))
        .collect();
    Json(APIResponse::Troupo(TroupoStatus {
        name: state.host_name.clone(),
        version: crate::version(),
        instances: inst_list,
        total_stats: InstanceStats::sum(&stats),
        host: fetch_host_status(state),
        available_satellites: sats,
    }))
}

pub fn rocket(
    state: Arc<AppState>,
    port: u16,
    bind: std::net::IpAddr,
) -> rocket::Rocket<rocket::Build> {
    let figment = rocket::Config::figment()
        .merge(("port", port))
        .merge(("address", bind));
    rocket::custom(figment).manage(state).mount(
        "/",
        routes![
            index,
            instance::get_instance,
            instance::put_instance,
            instance::delete_instance,
            instance::get_instance_env,
            instance::put_instance_env,
            instance::delete_instance_env,
            task::get_task,
            task::get_task_list,
            task::put_task,
            task::get_instance_task_list,
            task::post_instance_task,
        ],
    )
}
