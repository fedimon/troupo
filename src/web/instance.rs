use rocket::form::Form;
use rocket::serde::json::Json;
use rocket::State;
use std::sync::atomic::Ordering;

use super::auth::ApiAuth;
use super::types::*;
use crate::config::{InstanceConfig, Species};

#[get("/instance/<name>")]
pub fn get_instance(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
) -> Option<Json<APIResponse>> {
    let inst_config = state.get_instance(name)?;
    Some(Json(APIResponse::Instance(InstanceStatus::new(
        inst_config,
        name,
    ))))
}

#[put("/instance/<name>", data = "<form>")]
pub fn put_instance(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
    form: Form<InstancePut>,
) -> Option<Json<APIResponse>> {
    let inst_config = state.mut_instance(name, |i| {
        log::debug!("mutating instance {}", name);
        form.apply(i);
    });
    let inst_config = match inst_config {
        Some(i) => i,
        None => {
            // none found, it should be created
            log::debug!("creating instance {}", name);
            let local_id = state.instance_serial_id.fetch_add(1, Ordering::SeqCst);
            let mut inst = InstanceConfig::new(
                name.to_owned(),
                local_id,
                Species::default(),
                String::new(),
                String::new(),
            );
            form.apply_create(&mut inst);
            form.apply(&mut inst);

            state.runtime_config.mut_with(|c| {
                c.instances.insert(name.to_owned(), inst.clone());
            });

            inst
        }
    };

    Some(Json(APIResponse::Instance(InstanceStatus::new(
        inst_config,
        name,
    ))))
}

#[delete("/instance/<name>")]
pub fn delete_instance(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
) -> Option<Json<APIResponse>> {
    let inst_config = state.get_instance(name)?;

    state.del_instance(name);

    Some(Json(APIResponse::Instance(InstanceStatus::new(
        inst_config,
        name,
    ))))
}

#[get("/instance/<name>/env/<key>")]
pub fn get_instance_env(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
    key: &str,
) -> Option<Json<APIResponse<InstanceEnv>>> {
    let inst_config = state.get_instance(name)?;
    let value = inst_config.env.get(key);
    Some(Json(APIResponse::InstanceEnv(InstanceEnv {
        key: key.to_owned(),
        value: value.map(ToOwned::to_owned),
    })))
}

#[put("/instance/<name>/env/<key>", data = "<form>")]
pub fn put_instance_env(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
    key: &str,
    form: Form<InstanceEnvPut>,
) -> Option<Json<APIResponse<InstanceEnv>>> {
    let inst_config = state.mut_instance(name, |inst| {
        inst.env.insert(key.to_owned(), form.value.clone());
    })?;
    let value = inst_config.env.get(key);
    Some(Json(APIResponse::InstanceEnv(InstanceEnv {
        key: key.to_owned(),
        value: value.map(ToOwned::to_owned),
    })))
}

#[delete("/instance/<name>/env/<key>")]
pub fn delete_instance_env(
    state: &State<WebState>,
    _key: ApiAuth,
    name: &str,
    key: &str,
) -> Option<Json<APIResponse<InstanceEnv>>> {
    let inst_config = state.mut_instance(name, |inst| {
        inst.env.remove(key);
    })?;
    let value = inst_config.env.get(key);
    Some(Json(APIResponse::InstanceEnv(InstanceEnv {
        key: key.to_owned(),
        value: value.map(ToOwned::to_owned),
    })))
}
