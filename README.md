# troupo

a daemon to herd mastodons and other animals.

In short, it manages processes and environment on a machine
with a little API to query their status, statistics, and schedule tasks
to be run in the background.
It's very similar to software like *hashicorp nomad* or any *dockerd*, but
specialized for the software we host and their common points.

It can synchronize tasks with a central registry to be part of a larger
orchestration and automation platform.  
It uses systemd for process management, ensuring all processes can live beyond
a running troupo daemon. It can be shut down or broken, and the host and its
processes would stay operational, albeit immutable.  
It uses *hashicorp vault* to store secrets, so they are securely shared and
versionned between any number of troupo instances.  

Supported software:

- Mastodon (requires an existing local installation)
- GoToSocial (will download and maintain a local copy per user)
- Pixelfed (will check out any version; might be broken with PgSQL)

All those have their own sets of dependencies that troupo does not fully manage
(called *satellites*);
it expects one or more postgresql servers (where it can manage databases and roles),
one or more elasticsearch servers (optionally),
many redis servers (one pre-allocated per instance),
and SMTP credentials.  
It also does not manage a proxy, that you're supposed to build along with the
registry troupo synchronizes to.

In the future it could be made to use containers instead of attempting to
maintain local installations of supported software, or become an even thinner
wrapper around a container orchestration service.

in a way it's a minimal platform to run the code in `src/species/` .  

why the name: *psq c 1 troupo de mastodons*

> docker? heck no i don't like er!  
> hashicorp? i don't have a good reason not to use nomad again. idk. corporate vibes  
> but most of all, i will never kube her nets

### how it all fits together (as an ASCII drawing)

```
             ____________________________________
           ,| lets formally call that a cluster  |,
   ,------------------------------------------------------,
   |                                                      |
   |     ____    |__re__|   /||||\        ,____           |
   |    / PG \   |__di__|   | ES |      ,#|smtp|          |
   |    \____/   |__s___|   ||||||      o------o          |
   |     _ _ _ _ _ _ _ _ _ _      _ _ _ _ _ _ _ _ _ _     |
   |   /  ...  troupo   ...  \  /  ...  troupo   ...  \   |
   |   |  and its mastodons  |  |  and its mastodons  |   |
   |   \ _ _ _ _ _ _ _ _ _ _ /  \ _ _ _ _ _ _ _ _ _ _ /   |
   |_____________|________________________|_______________|
                 |                        |
                 '-----------v------------'
        ,--------------------|---------------------,
        |   this registry im talking about         |
        |   also doubles as a website or whatev    |
        '------------------------------------------'
```

### todo

- export for pixelfed and gts
- stats for pixelfed and gts
- health checks
- continue tasks on restart (instead of restarting them; they're already running as a systemd unit)
- support other softwares, isolate mastodon specific code, etc


## Usage

Below, *Instance* is used a lot to mean
*an instance of a supported software, running on this machine and managed by this troupo process*.
It can be thought of as a set of containers or processes, and settings to give them.

### HTTP API Endpoints

```
_Methods_       _Path_                      _Return_Type_
GET             /                           -> status
# instances
GET/PUT/DELETE  /instance/{name}            -> instance
GET/PUT/DELETE  /instance/{name}/env/{key}  -> env
POST            /instance/{name}/task       -> task
# tasks
GET             /task                       -> task list
GET             /task/{uuid}                -> task
PUT             /task/{uuid}                -> task
```

### API Client

A bash script to interact with a troupo instance exists as `dog`.  
It requires an authentication token as `$TROUPO_AUTH`.

```
export TROUPO_AUTH=...
./dog get
./dog put  instance/test       name=ectodon version=mastodon-glitch env.SINGLE_USER_MODE=true
./dog put  instance/test/env/MAX_TOOT_CHARS value=3333
./dog post instance/test/task  task_type=deploy
./dog post instance/test/task  task_type=initialize
./dog post instance/test/task  task_type=command parameters.cmd="tootctl media remove --days 120"
./dog get  task/$uuid
./dog get  instance/test
```

### Instance env/parameters

env variables for mastodon are in uppercase, while parameters for
troupo or relevant to the proxy or backups are in lowercase.

| Key                     | Description                               |
| ----------------------- | ----------------------------------------- |
| replace_domain          |                                           |
| aliases                 | , delimited string of additional domains or subdomains |
| search                  | boolean to enable elasticsearch support   |
| media_bucket            | maps to S3_BUCKET                         |
| media_host              | maps to S3_HOST; specific S3 end point    |
| media_proxy             | maps to CLOUDFRONT_HOST; host to proxy on.|
| onion_address           | optional address to an .onion             |
| LIMITED_FEDERATION_MODE | passthrough                               |
| MAX_TOOT_CHARS          | passthrough                               |
| AUTHORIZED_FETCH        | passthrough                               |
| AWS_SECRET_ACCESS_KEY   | passthrough (required)                    |
| AWS_ACCESS_KEY_ID       | passthrough (required)                    |

### Instance task types

- `deploy`: build env files and systemd services
- `check`: same, but without writing changes.
- `remove`: undo `deploy` but leave data untouched (pg, redis, s3, vault)
- `initialize`: create initial db structure
- `create_user`: create a user with `username` and `email` parameters
- `promote_user`: sets the permissions of an existing user (as `username`) to admin.
- `command`: run any command as the mastodon user and instance environment
- `prepare_upgrade`: start an upgrade procedure by running migrations, towards the currently set `.version` value.
- `finish_upgrade`: clears cache and finishes applying migrations. tu be ran after `prepare_upgrade` and every node has been deployed to the new version

an upgrade works with this flow:
- install a new software version on every troupo, update `versions`, and restart troupo. do Not deploy then
- run `prepare_upgrade` on a single troupo host. it may partially run migrations
- run `deploy` on every host to switch all background processes to the new version
- run `finish_upgrade` on a single troupo. it should finish applying migrations


### Configuration

| Path                       | Description                              |
| -------------------------- | ---------------------------------------- |
| /etc/troupo/troupo.toml    | read-only local startup configuration    |
| /etc/troupo/Rocket.toml    | ROCKET_CONFIG                            |
| /etc/troupo/instances.ron  | saved on change, keeps state. do not edit while it's running! format might break between updates. |
| /etc/troupo/env/{name}     | rendered env vars, passed to systemd     |


##### troupo.toml:

```toml
# locally available software
# when a unit uses an other/older path, it will switch to those on the next deploy
[versions.mastodon]
mastodon-latest = "/home/mastodon-latest/live/"
mastodon-glitch = "/home/mastodon-glitch/live/"

[api_keys]
semrBeuQVYGcnouQ6HpD = "test key label"

# to fetch tasks from and push progress to
[task_registry]
url = "https://maastodon.net/api/tasks/"
auth = "secret key"

[export]
host = "http://minio:9000"
key_id = ""
secret = ""
region = ""
bucket_name = "exports"

[bin_paths]
# it will look it up in $PATH if not set here
# ~ will also be replaced
bundle = "~/.local/share/gem/ruby/2.7.0/bin/bundle"

[common_env]
# global overrides

[satellites.postgres.default]
host = "localhost"
admin_user = "postgres"
admin_password = "postgres password"
db_prefix = "mastodon_"
user_prefix = "mastodon_"

[satellites.storage.default]
host = "s3.example.org"
region = "lo1"

[satellites.redis.default]
host = "localhost"
namespace = "masto_"

[satellites.mail.default]
host = "127.0.0.1"
port = 25
login = "user@example.com"
password = "example"

[satellites.elasearch.default]
host = "127.0.0.1"
```

### local requirements

common:
```
apt install zstd gnupg postgresql-client
```

for pixelfed:
```
apt install php7-fpm
```

that's very incomplete; please refer to `docker/Dockerfile` for a minimal test environment
